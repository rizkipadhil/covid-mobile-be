<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\fileTraits;

use App\Traits\globalTraits;

class file extends Model
{
  use fileTraits, globalTraits;

  // Title Page
  static public function nametag(){
      return ucwords('File Center');
  }

  protected $fillable = [
    'filename',
    'extension'
  ];

  // content table just copy from fillable
  static public function dataTable() {
      return ['filename','extension'];
  }

  // register here, any models have relation with this model will delete when data form this model removed.
  // e.g ['user'=>'hasMany', 'user'=>'hasOne']
  static public function arrayRelationDeleted() {
      return [];
  }

  static public function rules($id=NULL)
  {
      return [
          'filename' => 'required',
          'extension' => 'required'
      ];
  }
  public function file()
  {
      return $this->morphTo();
  }
}
