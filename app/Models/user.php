<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\globalTraits;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class user extends Model
{
  use globalTraits;
  protected $table = 'users';
  
  // Title Page
  static public function nametag(){
    return ucwords('data user');
  }

  // fillable
  protected $fillable = ['name','email','slug','password'];

  // content table just copy from fillable
    static public function dataTable() {
        return ['name','email','password'];
    }

    static public function arrayRelationDeleted()
    {
      return [
        'riwayat',
        'toko',
        'acara',
      ];
    }

  // Rules Attribut
    static public function rules($id=NULL)
    {
        // $fileRequeired = $id == NULL ? 'required' : 'nullable';
        return [
            'name'=>'required',
            'email'=>'required|string|email|max:255|unique:users,email,'.$id,
            'password'=>'required|string|min:6|confirmed',
            'foto' => 'nullable|mimes:jpeg,bmp,png,PNG,JPG,webp'
        ];
    }

    // Custom Message
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp'),
            'confirmed' => 'Password harus sama',
            'unique' => 'Email sudah terdaftar'
        ];
    }

  // Relation With File
  public function file(){
    return $this->morphMany('App\Models\file','file');
  }
  public function riwayat(){
    return $this->morphMany('App\Models\riwayat','riwayat');
  }
  public function riwayatrelation()
  {
    return $this->hasMany('App\Models\user');
  }
  public function toko()
  {
    return $this->hasMany('App\Models\toko');
  }
  public function acara()
  {
    return $this->hasMany('App\Models\acara');
  }
}
