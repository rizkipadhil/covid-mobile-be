<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\file;

use App\Traits\globalTraits;

class riwayat extends Model
{
    // call global function
    use globalTraits;

    protected $table = 'riwayats';
    
    // Title Page
    static public function nametag(){
        return ucwords('Data riwayat');
    }

    // fillable
    protected $fillable = ['user_id','keterangan','slug','lat','lang'];

    // content table just copy from fillable
    static public function dataTable() {
        return ['user_id','keterangan', 'lat', 'lang'];
    }

    // register here, any models have relation with this model will delete when data form this model removed.
    // e.g ['user'=>'hasMany', 'user'=>'hasOne']
    static public function arrayRelationDeleted() {
        return [];
    }
    // data form 
    static public function dataForm(array $array = []){
        // Sample Data Form
        // [
        //     'name' => 'isi',
        //     'title' => 'isi',
        //     'form' => 'textarea',
        //     'placeholder' => 'Isi isi',
        // ],
        // [
        //     'name' => 'riwayatpendidikan',
        //     'title' => 'riwayat pendidikan',
        //     'form' => 'default',
        //     'default' => '-',
        //     'type' => 'hidden',
        // ],
        $data = [
            [
                'name' => 'keterangan',
                'title' => 'keterangan',
                'form' => 'textarea',
            ],
            [
                'name' => 'foto',
                'title' => 'foto',
                'form' => 'file',
                'accept' => 'image/x-png,image/gif,image/jpeg',
            ],
        ];
        return $data;
    }

    // registered relation
        // e.g
            // example public function user()
            // example {
            // example     return $this->belongsToMany('App\Models\user');
            // example     return $this->belongsTo('App\Models\user');
            // example     return $this->hasMany('App\Models\user');
            // example     return $this->hasOne('App\Models\user');
            // example }
        // end

        // Relation with File
            public function file(){
                return $this->morphMany('App\Models\file','file');
            }
            public function riwayat()
            {
                return $this->morphTo();
            }
            public function user()
            {
                return $this->belongsTo('App\Models\user');
            }
    // end

    // Rules Attribut
    static public function rules($id=NULL)
    {
        // add Condition if where $id not null.
            $fileRequeired = $id == NULL ? 'required' : 'nullable';
        // end

        return [
            'user_id' => 'required',
            'nama' => 'required',
            'keterangan' => 'required',
            'lat' => 'required',
            'lang' => 'required',
        ];
    }

    // Custom Message Rules
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp')
        ];
    }
}
