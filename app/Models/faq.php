<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\file;

use App\Traits\globalTraits;

class faq extends Model
{
    // call global function
    use globalTraits;

    protected $table = 'faqs';
    
    // Title Page
    static public function nametag(){
        return ucwords('Data FAQ');
    }

    // fillable
    protected $fillable = ['pertanyaan','jawaban','slug'];

    // content table just copy from fillable
    static public function dataTable() {
        return ['pertanyaan','jawaban'];
    }

    // register here, any models have relation with this model will delete when data form this model removed.
    // e.g ['user'=>'hasMany', 'user'=>'hasOne']
    static public function arrayRelationDeleted() {
        return [];
    }
    // data form 
    static public function dataForm(array $array = []){
        // Sample Data Form
        // [
        //     'name' => 'isi',
        //     'title' => 'isi',
        //     'form' => 'textarea',
        //     'placeholder' => 'Isi isi',
        // ],
        // [
        //     'name' => 'riwayatpendidikan',
        //     'title' => 'riwayat pendidikan',
        //     'form' => 'default',
        //     'default' => '-',
        //     'type' => 'hidden',
        // ],
        $data = [
            [
                'name' => 'pertanyaan',
                'title' => 'pertanyaan',
                'form' => 'textarea',
            ],
            [
                'name' => 'jawaban',
                'title' => 'jawaban',
                'form' => 'textarea',
            ],
            // [
            //     'name' => 'foto',
            //     'title' => 'foto',
            //     'form' => 'file',
            //     'accept' => 'image/x-png,image/gif,image/jpeg',
            // ],
        ];
        return $data;
    }

    // registered relation
        // e.g
            // example public function user()
            // example {
            // example     return $this->belongsToMany('App\Models\user');
            // example     return $this->belongsTo('App\Models\user');
            // example     return $this->hasMany('App\Models\user');
            // example     return $this->hasOne('App\Models\user');
            // example }
        // end

        // Relation with File
            public function file(){
                return $this->morphMany('App\Models\file','file');
            }
    // end

    // Rules Attribut
    static public function rules($id=NULL)
    {
        // add Condition if where $id not null.
            $fileRequeired = $id == NULL ? 'required' : 'nullable';
        // end

        return [
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ];
    }

    // Custom Message Rules
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp')
        ];
    }
}
