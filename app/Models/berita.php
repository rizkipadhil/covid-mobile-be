<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\file;

use App\Traits\globalTraits;

class berita extends Model
{
    // call global function
    use globalTraits;

    protected $table = 'beritas';
    
    // Title Page
    static public function nametag(){
        return ucwords('Data Berita');
    }

    // fillable
    protected $fillable = ['judul','isi'];

    // content table just copy from fillable
    static public function dataTable() {
        return ['judul','isi'];
    }

    // register here, any models have relation with this model will delete when data form this model removed.
    // e.g ['user'=>'hasMany', 'user'=>'hasOne']
    static public function arrayRelationDeleted() {
        return [];
    }

    // registered relation
        // e.g
            // example public function user()
            // example {
            // example     return $this->belongsToMany('App\Models\user');
            // example     return $this->belongsTo('App\Models\user');
            // example     return $this->hasMany('App\Models\user');
            // example     return $this->hasOne('App\Models\user');
            // example }
        // end

        // Relation with File
            public function file(){
                return $this->morphMany('App\Models\file','file');
            }
    // end

    // Rules Attribut
    static public function rules($id=NULL)
    {
        // add Condition if where $id not null.
            $fileRequeired = $id == NULL ? 'required' : 'nullable';
        // end

        return [
            'judul' => 'required',
            'isi' => 'required',
            'foto' => $fileRequeired.'|mimes:jpeg,bmp,png,PNG,JPG,webp'
        ];
    }

    // Custom Message Rules
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp')
        ];
    }
}
