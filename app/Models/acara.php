<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\file;

use App\User as user;

use App\Traits\globalTraits;

class acara extends Model
{
    // call global function
    use globalTraits;

    protected $table = 'acaras';
    
    // Title Page
    static public function nametag(){
        return ucwords('Data acara');
    }

    // fillable
    protected $fillable = ['nama','keterangan','slug', 'user_id', 'jumlahtamu'];

    // content table just copy from fillable
    static public function dataTable() {
        return ['nama','keterangan', 'user_id', 'jumlahtamu', 'jumlahtamu'];
    }

    // register here, any models have relation with this model will delete when data form this model removed.
    // e.g ['user'=>'hasMany', 'user'=>'hasOne']
    static public function arrayRelationDeleted() {
        return [];
    }
    // data form 
    static public function dataForm(array $array = []){
        // Sample Data Form
        // [
        //     'name' => 'isi',
        //     'title' => 'isi',
        //     'form' => 'textarea',
        //     'placeholder' => 'Isi isi',
        // ],
        // [
        //     'name' => 'riwayatpendidikan',
        //     'title' => 'riwayat pendidikan',
        //     'form' => 'default',
        //     'default' => '-',
        //     'type' => 'hidden',
        // ],
        $userdata = [];
        $alluser = user::get();
        foreach ($alluser as $item) {
            if ($item->roles->pluck('name')->implode(' ') == 'member'
            ) {
                $userdata[$item->id] = $item->name;
            }
        }
        $data = [
            [
                'name' => 'user_id',
                'title' => 'Pilih User',
                'dataselect' => $userdata,
                'form' => 'select',
            ],
            [
                'name' => 'nama',
                'title' => 'nama',
                'form' => 'text',
            ],
            [
                'name' => 'jumlahtamu',
                'title' => 'Jumlah Tamu',
                'form' => 'number',
            ],
            [
                'name' => 'keterangan',
                'title' => 'keterangan',
                'form' => 'textarea',
            ],
            // [
            //     'name' => 'foto',
            //     'title' => 'foto',
            //     'form' => 'file',
            //     'accept' => 'image/x-png,image/gif,image/jpeg',
            // ],
        ];
        return $data;
    }

    // registered relation
        // e.g
            // example public function user()
            // example {
            // example     return $this->belongsToMany('App\Models\user');
            // example     return $this->belongsTo('App\Models\user');
            // example     return $this->hasMany('App\Models\user');
            // example     return $this->hasOne('App\Models\user');
            // example }
        // end

        // Relation with File
            public function file(){
                return $this->morphMany('App\Models\file','file');
            }
            public function riwayat()
            {
                return $this->morphMany('App\Models\riwayat', 'riwayat');
            }
            public function user()
            {
                return $this->belongsTo('App\Models\user');
            }
    // end

    // Rules Attribut
    static public function rules($id=NULL)
    {
        // add Condition if where $id not null.
            $fileRequeired = $id == NULL ? 'required' : 'nullable';
        // end

        return [
            'nama' => 'required',
            'keterangan' => 'required',
            'jumlahtamu' => 'required',
        ];
    }

    // Custom Message Rules
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp')
        ];
    }
}
