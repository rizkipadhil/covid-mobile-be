<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\file;

use App\Traits\globalTraits;

class staticdata extends Model
{
    // call global function
    use globalTraits;
    
    // Title Page
    static public function nametag(){
        return ucwords('Data staticdata');
    }

    // fillable
    protected $fillable = [
        'name',
        'slug',
        'detail',
    ];

    // content table just copy from fillable
    static public function dataTable() {
        // status diterima / ditolak
        return [
            'name',
            'slug',
            'detail',
            ];
    }

    static public function fileUpload(){
        // list file yang di up
        return [];
    } 

    static public function withnamefileUpload(){
        // list file yang di up
        return [];
    } 

    static public function checkDataType(){
        return [];
    } 

    // register here, any models have relation with this model will delete when data form this model removed.
    // e.g ['user'=>'hasMany', 'user'=>'hasOne']
    static public function arrayRelationDeleted() {
        return [];
    }

    // registered relation
        // e.g
            // example public function user()
            // example {
            // example     return $this->belongsToMany('App\Models\user');
            // example     return $this->belongsTo('App\Models\user');
            // example     return $this->hasMany('App\Models\user');
            // example     return $this->hasOne('App\Models\user');
            // example }
        // end

        // Relation with File
            public function file(){
                return $this->morphMany('App\Models\file','file');
            }
    // Rules Attribut
    static public function rules($id=NULL)
    {
        // add Condition if where $id not null.
            // $fileRequeired = $id == NULL ? 'required' : 'nullable';
            $nameRequeired = $id == NULL ? 'required' : 'nullable';
        // end

        return [
            'name' => $nameRequeired,
            'slug' => 'nullable',
            'detail' => 'nullable',
            // 'foto' => $fileRequeired.'|mimes:jpeg,bmp,png,PNG,JPG,webp'
        ];
    }

    // Custom Message Rules
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp')
        ];
    }
}
