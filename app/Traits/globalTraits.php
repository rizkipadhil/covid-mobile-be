<?php

namespace App\Traits;

use App\Models\file;

use Carbon\Carbon;

trait globalTraits
{
    // Store Data
    static public function storingdata(array $data, $id = NULL) {
        try {
            // Split Data Array
            // Main     
            $arrayData = [];
            foreach (self::dataTable() as $column) {
                if (array_key_exists($column ,$data)) {
                    $arrayData[$column] = $data[$column];
                }
            }
            if ($id == NULL) {
                $arrayData['slug'] = \SetFunction::regexTimestamp(Carbon::now());
            }
            // end

            // Saving Data
            // Main
                // model query
                // add custom data to arrayData. You can add it for action Create or Update.
                    // E.g $arrayData['user_id'] = $data['user_id'];
                //end
                if ($id == NULL) {
                    // $arrayData['created_by'] = Auth::id(); 
                    $generate = self::create($arrayData);
                }else{
                    $generate = self::find($id);
                    // $arrayData['updated_by'] = Auth::id(); 
                    $generate->update($arrayData);
                    $generate->save();
                }
            // end 

            // fileupload (if required)
                if(array_key_exists('foto' ,$data)){
                    // check file if id not null
                        if($id !== NULL){
                            if ($generate->file->count() > 0) {
                                foreach ($generate->file as $files) {
                                    file::deleteFile($files->filename);
                                    $files->delete();
                                }
                            }
                        }
                    //end

                    // store file
                        $fileResult = file::storingUploadFile($data, 'foto');
                    // end

                    //fileupload query
                        if ($fileResult['status'] == '200') {
                            $generate->file()->create([
                                'filename' => $fileResult['filename'],
                                'extension' => $fileResult['extension']
                            ]);
                        }
                    // end
                }
            // end

            // generate result
            $result = [];
            $result['data'] = $generate;
            $result['data']['file'] = $generate->file;
            return $result;
        } catch (\Exception $e) {
            $cek = explode(' ' ,$e->getMessage());
            $result['status'] = 0;
            $result['error']['message'] = $e->getMessage();
            $result['error']['validasi'] = $cek[count($cek) - 1];
            return $result;
        }
    }

    // remove data
    static public function deletingdata($id, $model){
        $query = self::find($id);
        if (count(self::arrayRelationDeleted()) > 0) {
            self::deleteRelation($id, $model);
        }
        if(isset($query->file)){
            if ($query->file->count() > 0) {
                foreach ($query->file as $files) {
                    file::deleteFile($files->filename);
                    $files->delete();
                }
            }
        }
        return $query->delete();
    }

    // remove relation model
    static public function deleteRelation($id, $model)
    {
        $query = self::find($id);
        // each relation
        foreach (self::arrayRelationDeleted() as $relation => $key) {
            // check available data from relation.
            if (!empty($query->$relation)) {
                // call relation data
                $modeldataRemove = 'App\Models\\'.$relation;
                if ($key == 'hasOne') {
                    // hasOne
                    $dataRemove =  $modeldataRemove::where($model.'_id', $id)->first();
                    $dataRemove->deletingdata($dataRemove->id, $relation);
                }else {
                    // hasMany
                    $dataRemove =  $modeldataRemove::where($model.'_id', $id);
                    foreach ($dataRemove->get() as $data) {
                        $modeldataRemove::find($data->id)->deletingdata($data->id, $relation);
                    }
                }
            }
        }
    }
}
