<?php

namespace App\Traits;

trait fileTraits
{
  // Store File without modeldata
  static public function storingUploadFile(array $data, $name)
  {
      $file = $data[$name];
      $fileName   = md5($file->getClientOriginalName().date("Y-m-d H:i:s")).'.'.$file->getClientOriginalExtension();
      $moved = $file->move('uploads/', $fileName);
      if ($moved) {
          $result = ['status' => '200', 'filename'=> $fileName, 'extension'=> $file->getClientOriginalExtension()];
          return $result;
      }else {
          return response()->json(['status' => 401,'message' => 'Gambar Gagal Diupload',], 401, [], JSON_PRETTY_PRINT);
          die();
      }
  }

  // store file with modeldata
  static public function storingFile(array $data, $namearray, $modeldata, $id)
  {
    // get array file
    $file       = $data[$namearray];

    // save data
    $fileName   = md5($file->getClientOriginalName().date("Y-m-d H:i:s")).'.'.$file->getClientOriginalExtension();
    $moved = $file->move('uploads/', $fileName);

    // set model data
    $model = 'App\Models\\'.$modeldata;
    if(!is_subclass_of($model, 'Illuminate\Database\Eloquent\Model')){
      return redirect('errors');
    }

    // query data
    $query = $model::findOrFail($id);

    // store data
    $file = $query->file()->create([
      'filename' => $fileName,
      'extension' => $file->getClientOriginalExtension()
    ]);

    // set result data
    $result = [];
    $result['data'] = $query;
    $result['data']['file'] = $file;
    return $result;
  }
  // remove with id
  static public function removeFile($id)
  {
    $data = self::findOrFail($id);
    $path =  'uploads/';
    $removefile = \File::delete($path.$data->filename);
    $remove = $data->delete();
    return 'Success Delete';
  }
  // remove with filename
  static public function deleteFile($filename) {
    $path =  'uploads/';
    if (\File::delete($path.$filename)) {
      return 'Success Delete';
    }
  }
}
