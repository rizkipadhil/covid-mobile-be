<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ctview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ctview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk Happy Happy ja heh !';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses HEH .....');
        $dirmodel    = public_path('../app/Models');
        $dircontroller    = public_path('../app/Http/Controllers/Admin/View');
        $model = scandir($dirmodel);
        $controller = scandir($dircontroller);
        $dataheh = [];
        foreach ($model as $key => $value) {
            $explode = explode('.' ,$value);
            if($explode[0] !== ''){
                $search = array_search($explode[0].'Controller.php', $controller, true);
                if(!$search){
                    //make controllers
                    $nameController = "View\\".$explode[0];
                    $this->call('make:controller', [
                        'name' => $explode[0] .'Controller',
                        '--model' => $explode[0]
                    ]);
                    $dataheh[]= $explode[0];
                    //view
                    $this->copyView($explode[0]);
                    $this->makeDataRole($explode[0]);
                } 
            }
        }
        $this->info('Success Create HEH!');
    }
    function copyView($name){
      $file_location = base_path() . '/resources/views/view/admin/' . $name;
      exec('cp -r '. $this->getStubView() .' '. $file_location);
    }
    function makeDataRole($name){
        $controller = [
            'index',
            'create',
            'view',
            'update',
            'delete'
        ];
        $userrole = [
            '1',
        ];
        foreach ($userrole as $setrole) {
            foreach ($controller as $permission) {
                if ($setrole == '1') {
                    foreach ($controller as $permission) {
                        $this->setPermission($setrole, $name.'-'.$permission);
                    }
                }else{
                    $this->setPermission($setrole, $name.'-index');
                    $this->setPermission($setrole, $name.'-create');
                    $this->setPermission($setrole, $name.'-view');
                    $this->setPermission($setrole, $name.'-update');
                }
            }
        }
    }
    protected function setPermission($role,  $name){
        try {
            $permission = Permission::where('name', '=', $name)->first();
            if(empty($permission)){
                $permission = new Permission();
                $permission->name = $name;
                $permission->save();
            }
            $r = Role::find($role);
            $r->givePermissionTo($permission);
            return true;
        } catch (Exception $e) {
            report($e);
            return false;
        }
    }
    protected function getStubView()
    {
    	return  base_path() . '/resources/stubs/viewcustom/';
    }
}
