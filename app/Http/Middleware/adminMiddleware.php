<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

use Alert;

class adminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->roles ? Auth::user()->roles->pluck('name')->implode(' ') : 'admin';
        return $next($request);
        // if ($role == 'superadmin' || $role == 'admin') {
        // } else {
            // Auth::logout();
            // return redirect('/login');
        // }
    }
}
