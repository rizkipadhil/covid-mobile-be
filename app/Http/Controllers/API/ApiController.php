<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\riwayat;
use App\Models\faq;
use Response;
use Hash;
use Validator;
use Carbon\Carbon;

class ApiController extends ResponseController
{
    function bantuan(){
        try {
            $data = faq::latest()->get();
            $success['data'] =  $data;
            $success['message'] = "LIST_BANTUAN";
            $success['code'] = 200;
            return $this->sendResponse($success);
        } catch (\Exception $e) {
            $cek = explode(' ', $e->getMessage());
            $result['status'] = 401;
            $result['error']['message'] = $e->getMessage();
            $result['error']['validasi'] = $cek[count($cek) - 1];
            return $this->sendResponse($result);
        }
    }
    function riwayatUser(){
        $buildata = [];
        // $data = riwayat::where('user_id', Auth::user()->id)->latest()->limit(10)->get();
        // $datablaik = riwayat::where('riwayat_id', Auth::user()->id)->where('riwayat_type', 'App\Models\user')->latest()->limit(10)->get();
        $data = riwayat::latest()->get();
        foreach ($data as $item) {
            if(count($buildata) <= 10){
                if($item->user_id == Auth::user()->id){
                    $keterangan = 'Bertemu';
                    if ($item->keterangan == 'acara') {
                        $keterangan = 'Menghadiri';
                    }elseif($item->keterangan == 'toko') {
                        $keterangan = 'Mengunjungi';
                    }
                    $callData = $item->keterangan == 'user' ? 'name' : 'nama';
                    $buildata[] = [
                        "id" => $item->id,
                        "status" => $keterangan,
                        "action" => ucwords($item->riwayat[$callData]),
                        "date" => \SetFunction::setDateID($item->created_at),
                        "time" => \Carbon\Carbon::parse($item->created_at)->format('H:i'),
                        "lat" => $item->lat,
                        "lang" => $item->lang,
                    ];
                }
                if($item->riwayat_id == Auth::user()->id && $item->riwayat_type == 'App\Models\user'){
                    $keterangan = 'Bertemu';
                    $modelcall = 'user';
                    if ($item->keterangan == 'acara') {
                        $keterangan = 'Menghadiri';
                        $modelcall = 'acara';
                    }elseif($item->keterangan == 'toko') {
                        $keterangan = 'Mengunjungi';
                        $modelcall = 'toko';
                    }
                    $callData = $item->keterangan == 'user' ? 'name' : 'nama';
                    $buildata[] = [
                        "id" => $item->id,
                        "status" => $keterangan,
                        "action" => ucwords($item[$modelcall][$callData]),
                        "date" => \SetFunction::setDateID($item->created_at),
                        "time" => \Carbon\Carbon::parse($item->created_at)->format('H:i'),
                        "lat" => $item->lat,
                        "lang" => $item->lang,
                    ];
                }
            }
        }
        $success['data'] =  $buildata;
        $success['riwayat'] =  $data;
        $success['message'] = "LIST_RIWAYAT";
        $success['code'] = 200;
        return $this->sendResponse($success);
    }
    function actionuser($status, $modelid, Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'lang' => 'required',
                'lat' => 'required',
            ]);
            if ($validator->fails()) {
                $error['message'] = "ACTION_USER_FAIL";
                $error['code'] = 401;
                $error['data'] = $validator->errors();
                return $this->sendResponse($error);
            }
            $modeldata = 'App\Models\\' . $status;
            $data =  $modeldata::where('slug', $modelid)->first();
            $riwayat = $data->riwayat()->create([
                'user_id' => Auth::user()->id,
                'lang' => $request->lang,
                'lat' => $request->lat,
                'keterangan' => $status,
                'slug' => \SetFunction::regexTimestamp(Carbon::now()),
            ]);
            $success['data'] =  $data;
            $success['riwayat'] =  $riwayat;
            $success['message'] = "ACTION_USER_SUCCESS";
            $success['code'] = 200;
            return $this->sendResponse($success);
        } catch (\Exception $e) {
            $cek = explode(' ', $e->getMessage());
            $result['status'] = 401;
            $result['error']['message'] = $e->getMessage();
            $result['error']['validasi'] = $cek[count($cek) - 1];
            return $this->sendResponse($result);
            // return $this->sendError($result, 401);
        }
    }
}