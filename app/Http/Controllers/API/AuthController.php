<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Carbon\Carbon;
use Exception;

class AuthController extends ResponseController
{
    protected $model;
    protected $url;

    public function __construct(user $model)
    {
        $this->model = $model;
        $this->url = 'user';
    }
    //create user
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), $this->model->rules(), $this->model->customMessage());
        if ($validator->fails()) {
            $error['message'] = "REGISTER_FAIL";
            $error['code'] = 401;
            $error['data'] = $validator->errors();
            return $this->sendError($error);
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['slug'] = \SetFunction::regexTimestamp(Carbon::now());
        $data = $this->model->create($input);
        $role_r = Role::where('id', '=', 3)->firstOrFail();
        $data->assignRole($role_r); //Assigning role to user  
        if ($data) {
            $success['token'] =  user::find($data->id);
            $success['message'] = "REGISTER_SUCCESS";
            $success['code'] = 200;
            return $this->sendResponse($success);
        } else {
            $error['message'] = "REGISTER_FAIL";
            $error['code'] = 401;
            return $this->sendError($error, 401);
        }
    }

    //login
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $error['message'] = "LOGIN_FAIL";
            $error['code'] = 401;
            $error['data'] = $validator->errors();
            return $this->sendError($error);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (!Auth::attempt($credentials)) {
            $error['message'] = "UNAUTHORIZED";
            $error['code'] = 401;
            return $this->sendError($error, 401);
        }
        $user = $request->user();
        $success['data'] = $user;
        $success['message'] = "LOGIN_SUCCESS";
        $success['code'] = 200;
        $success['token'] =  $user->createToken('token')->accessToken;
        $role = Auth::user()->roles ? Auth::user()->roles->pluck('name')->implode(' ') : 'member';
        $success['role'] = $role;
        return $this->sendResponse($success);
    }

    //logout
    public function logout(Request $request)
    {
        
        $isUser = $request->user()->token()->revoke();
        if($isUser){
            $success['message'] = "Successfully logged out.";
            return $this->sendResponse($success);
        }
        else{
            $error = "Something went wrong.";
            return $this->sendResponse($error);
        }
            
        
    }

    //getuser
    public function getUser(Request $request)
    {
        //$id = $request->user()->id;
        $user = $request->user();
        if($user){
            return $this->sendResponse($user);
        }
        else{
            $error = "user not found";
            return $this->sendResponse($error);
        }
    }
    public function getDataBarcode($user)
    {
        $data = User::where('slug', $user)->first();
        if(empty($data)){
            $error['message'] = "USER_NOT_FOUND";
            $error['code'] = 401;
            return $this->sendError($error, 401);
        }
        $success['token'] =  user::find($data->id);
        $success['message'] = "USER_FOUND";
        $success['code'] = 200;
        return $this->sendResponse($success);
    }
}