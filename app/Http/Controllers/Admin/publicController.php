<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\user;
use App\Models\riwayat;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;
use Auth;
use Alert;
use Redis;

class publicController extends Controller
{
    public function tracking(){
        return view('view.admin.track.index');
    }
    public function trackresult(Request $request){
        $request = $request->all();
        $data = riwayat::with('riwayat','user')->whereHas('user', function (Builder $query)  use ($request) {
                    $query->where('noktp', $request['nik']);
                });
        $data->whereDate('created_at', '>=', $request['tanggalawal']);
        $data->whereDate('created_at', '<=', $request['tanggalakhir']);
        $datas = $data->orderBy('created_at', 'desc')->get();
        return view('view.admin.track.result', compact('request', 'datas'));
    }
    public function setRedis(){
        $redis = Redis::connection();
        $message = 'Ada Notifikasi baru!';
        $redis->publish('message', $message);
        return response()->json($message,200);
    }
    public function storeFile($id, $model, $namearray, Request $request) {
      $save = \App\Models\file::storingFile($request->all(), $namearray, $model, $id);
      $result = [
                  'status' => 200,
                  'message' => "Upload Berhasil",
                  'data' => $save,
                ];
      return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }
    public function deleteFile($id, Request $request)
    {
        $remove =  \App\Models\file::removeFile($id);
        if (!empty($request->json)) {
            $result = [
            'status' => 200,
            'message' => $remove,
            ];
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        }else {
            return redirect()->back();
        }
    }
    public function settings()
    {
        return view('settings.index');
    }
    public function changeProfil(Request $request){
        $id = Auth::user()->id;
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|string|email|max:255|unique:users,email,'.$id,
            'foto' => 'nullable|mimes:jpeg,bmp,png,PNG,JPG,webp'
        ], [
            'required' => ucwords('Harap isi :attribute'),
            'confirmed' => 'Password harus sama',
            'unique' => 'Email sudah terdaftar'
        ]);
        $data = user::storingdata($request->all(), $id);
        if(isset($data['error'])){
            return redirect()->back()->with('errorProgram', $data);
        }else{
            Alert('','Berhasil Ubah Data','success');
            return redirect()->back();
        }
    }
    public function changePassword(Request $request){
        $this->validate($request, [
            'password'=> 'required|string|min:6|confirmed',
        ], [
            'required' => ucwords('Harap isi :attribute'),
            'confirmed' => 'Password harus sama'
        ]);
        $id = Auth::user()->id;
        $data = user::find($id);
        $data->password = Hash::make($request->password);
        $data->save();
        Alert('','Berhasil Mengubah Password','success');
        return redirect()->back();
    }
    public function publishData($model, $id, $status, Request $request)
    {
        try {
            $modeldata = 'App\Models\\'.$model;
            $data =  $modeldata::findOrFail($id);
            if ($model == 'berita') {
                $data->publish = $status;
            }else{
                $data->status = $status;
                if ($status == 'tolak') {
                    $data->keterangantolak = $request->keterangantolak;
                }
            }
            $data->save();
            if (!empty($request->json)) {
                $result = [
                    'status' => 200,
                    'data' => $data,
                ];
                return response()->json($result, 200, [], JSON_PRETTY_PRINT);
            }else {
                if (isset($request->urlredirect)) {
                    return redirect($request->urlredirect);
                }else{
                    return redirect()->back();
                }
            }
        } catch (\Exception $e) {
            $cek = explode(' ' ,$e->getMessage());
            $result['status'] = 0;
            $result['error']['message'] = $e->getMessage();
            $result['error']['validasi'] = $cek[count($cek) - 1];
            return $result;
        }
    }
}
