<?php

namespace App\Http\Controllers\Admin\View;

use App\Http\Controllers\Controller;
use App\Models\toko;
use Auth;
use Illuminate\Http\Request;

class tokoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    protected $url;

    public function __construct(toko $model)
    {
        $this->model = $model;
        $this->url = 'toko';
        $this->middleware('permission:'.$this->url.'-index', ['only'=> ['index']]);
        $this->middleware('permission:'.$this->url.'-create', ['only'=> ['create', 'store']]);
        $this->middleware('permission:'.$this->url.'-view', ['only'=> ['show']]);
        $this->middleware('permission:'.$this->url.'-update', ['only'=> ['edit', 'update']]);
        $this->middleware('permission:'.$this->url.'-delete', ['only'=> ['destroy']]);
    }

    public function index(Request $request)
    {
        $query = $this->model->orderBy('updated_at', 'desc');
        $role = Auth::user()->roles ? Auth::user()->roles->pluck('name')->implode(' ') : 'member';
        if ($role == 'admin' || $role == 'superadmin') {
            $query->whereHas('user');
        }
        if ($role == 'member') {
            $query->where('user_id', Auth::user()->id);
        }
        $datas = $query->orderBy('updated_at', 'desc')->get();
        $nametag = $this->model->nametag();
        $status = $this->url;
        return view('view.admin.'.$this->url.'.index', compact('datas','nametag','status'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        $status = $this->url;
        $nametag = $this->model->nametag();
        $form = $this->model->dataForm();
        return view('view.admin.'.$this->url.'.create', compact('status','nametag','form'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $status = $this->url;
        $this->validate($request, $this->model->rules(), $this->model->customMessage());
        $data = $this->model->storingdata($request->all());
        if(isset($data['error'])){
            return redirect()->back()->with('errorProgram', $data);
        }else{
            Alert('','Berhasil Simpan Data','success');
            return redirect('admin/'.$this->url);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $id)
    {
        $status = $this->url;
        $data = $this->model->find($id);
        if (empty($data)) {
            return redirect('errors');
        }
        $nametag = $this->model->nametag();
        return view('view.admin.'.$this->url.'.show', compact('status','data','nametag'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $status = $this->url;
        $data = $this->model->find($id);
        if (empty($data)) {
            return redirect('errors');
        }
        $nametag = $this->model->nametag();
        $form = $this->model->dataForm();
        return view('view.admin.'.$this->url.'.edit', compact('status','data','nametag','form'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $status = $this->url;
        $data = $this->model->find($id);
        if (empty($data)) {
        return redirect('errors');
        }
        $rules = $this->model->rules($id);
        $this->validate($request, $rules, $this->model->customMessage());
        $data = $this->model->storingdata($request->all(), $id);
        if(isset($data['error'])){
            return redirect()->back()->with('errorProgram', $data);
        }else{
            Alert('','Berhasil Ubah Data','success');
            return redirect('admin/'.$this->url);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        $status = $this->url;
        $data = $this->model->deletingdata($id, $status);
        Alert('','Berhasil Menghapus Data','success');
        return redirect()->back();
    }
}
