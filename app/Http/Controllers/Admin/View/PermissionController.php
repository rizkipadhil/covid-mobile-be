<?php
namespace App\Http\Controllers\Admin\View;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

class PermissionController extends Controller {
    public function __construct(Permission $model)
    {
        $this->url = 'permission';
        $this->model = $model;
        $this->status = 'permission';
        $this->nametag = ucwords('data permission');
        $this->middleware('permission:'.$this->url.'-index', ['only'=> ['index']]);
        $this->middleware('permission:'.$this->url.'-create', ['only'=> ['create', 'store']]);
        $this->middleware('permission:'.$this->url.'-view', ['only'=> ['show']]);
        $this->middleware('permission:'.$this->url.'-update', ['only'=> ['edit', 'update']]);
        $this->middleware('permission:'.$this->url.'-delete', ['only'=> ['destroy']]);
    }

    public function index() 
    {
        $datas = Permission::all();
        $status = $this->status;
        $nametag = $this->nametag;
        return view('view.admin.'.$this->url.'.index', compact('nametag', 'status', 'datas'));
    }
    
    public function create() 
    {
        $roles = Role::get();
        $status = $this->status;
        $nametag = $this->nametag;
        return view('view.admin.'.$this->url.'.create', compact('nametag', 'status', 'roles'));
    }

    public function store(Request $request) 
    {
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) { //If one or more role
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail();
                $permission = Permission::where('name', '=', $name)->first();
                $r->givePermissionTo($permission);
            }
        }
        Alert('','Berhasil Menyimpan Data','success');

        return redirect()->route('permission.index');
    }

    public function show($id) 
    {
        // @TODO
        return redirect('permission');
    }

    public function edit($id) 
    {
        $data = Permission::findOrFail($id);
        $status = $this->status;
        $nametag = $this->nametag;
        return view('view.admin.'.$this->url.'.edit', compact('nametag', 'status', 'data'));
    }

    public function update(Request $request, $id) 
    {
        $data = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();
        
        Alert('','Berhasil Menyimpan Data','success');
        return redirect()->route('permission.index');
    }

    public function destroy($id) 
    {
        $permission = Permission::findOrFail($id);

        if ($permission->name == "Administer roles & permission") {
            Alert('','Gagal Menghapus Data','error');
            return redirect()->back();
        }

        $permission->delete();

        Alert('','Berhasil Menghapus Data','success');
        return redirect()->back();
    }
}