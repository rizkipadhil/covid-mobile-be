<?php

namespace App\Http\Controllers\Admin\View;

use App\User as user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Alert;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  protected $model;
  protected $url;

  public function __construct(user $model)
  {
    $this->model = $model;
    $this->url = 'user';
    $this->middleware('permission:'.$this->url.'-index', ['only'=> ['index']]);
    $this->middleware('permission:'.$this->url.'-create', ['only'=> ['create', 'store']]);
    $this->middleware('permission:'.$this->url.'-view', ['only'=> ['show']]);
    $this->middleware('permission:'.$this->url.'-update', ['only'=> ['edit', 'update']]);
    $this->middleware('permission:'.$this->url.'-delete', ['only'=> ['destroy']]);
  }

  public function index(Request $request)
  {
    $datas = $this->model->get();
    $nametag = $this->model->nametag();
    $status = $this->url;
    $role = $request->role;
    return view('view.admin.'.$this->url.'.index', compact('datas','nametag','status', 'role'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    $status = $this->url;
    $nametag = $this->model->nametag();
    $roles = Role::all();
    $role = $request->role;
    return view('view.admin.'.$this->url.'.create', compact('status','nametag','roles','role'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $status = $this->url;
    $this->validate($request, $this->model->rules(), $this->model->customMessage());
    $input = $request->all();
    $input['password'] = Hash::make($input['password']);
    $data = $this->model->create($input);
    $roles = $request->roles;
    if (isset($roles)) {
      foreach ($roles as $role) {
        $role_r = Role::where('id', '=', $role)->firstOrFail();            
        $data->assignRole($role_r); //Assigning role to user
      }
    }      
    if(isset($data['error'])){
        return redirect()->back()->with('errorProgram', $data);
    }else{
        Alert('','Berhasil Simpan Data','success');
        return redirect('admin/'.$this->url.'?role='.$request->redirect);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    $status = $this->url;
    $data = $this->model->find($id);
    if (empty($data)) {
      return redirect('errors');
    }
    $nametag = $this->model->nametag();
    $role = $request->role;
    return view('view.admin.'.$this->url.'.show', compact('status','data','nametag','role'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id, Request $request)
  {
    $status = $this->url;
    $data = $this->model->find($id);
    if (empty($data)) {
      return redirect('errors');
    }
    $roles = Role::all();
    $nametag = $this->model->nametag();
    $role = $request->role;
    return view('view.admin.'.$this->url.'.edit', compact('status','data','nametag','roles','role'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $status = $this->url;
    $data = $this->model->find($id);
    if (empty($data)) {
      return redirect('errors');
    }
    $rules = $this->model->rules($id);
    $this->validate($request, $rules, $this->model->customMessage());
    $input = $request->all();
    $input['password'] = Hash::make($input['password']);
    $save = $data->update($input);
    $roles = $request->roles;

    if (isset($roles)) { 
      $data->roles()->sync($roles);          
    } else {
      $data->roles()->detach();
    }
    if(isset($data['error'])){
        return redirect()->back()->with('errorProgram', $data);
    }else{
        Alert('','Berhasil Ubah Data','success');
        return redirect('admin/'.$this->url.'?role='.$request->redirect);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $status = $this->url;
    $data = $this->model->deletingdata($id, $status);
    Alert('','Berhasil Menghapus Data','success');
    return redirect()->back();
  }
}
