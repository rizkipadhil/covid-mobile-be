<?php

namespace App\Http\Controllers\Admin\View;

use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Alert;

class RoleController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  protected $model;
  protected $url;

  public function __construct(Role $model)
  {
    $this->model = $model;
    $this->url = 'role';
    $this->nametag = 'Role';
    $this->middleware('permission:'.$this->url.'-index', ['only'=> ['index']]);
    $this->middleware('permission:'.$this->url.'-create', ['only'=> ['create', 'store']]);
    $this->middleware('permission:'.$this->url.'-view', ['only'=> ['show']]);
    $this->middleware('permission:'.$this->url.'-update', ['only'=> ['edit', 'update']]);
    $this->middleware('permission:'.$this->url.'-delete', ['only'=> ['destroy']]);
  }

  public function index(Request $request)
  {
    $datas = $this->model->get();
    $nametag = $this->nametag;
    $status = $this->url;
    return view('view.admin.'.$this->url.'.index', compact('datas','nametag','status'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    $status = $this->url;
    $nametag = $this->nametag;
    $permissions = Permission::all();
    return view('view.admin.'.$this->url.'.create', compact('status','nametag', 'permissions'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $status = $this->url;
    $this->validate($request, [
        'name'=>'required|unique:roles|max:10',
        'permissions' =>'required',
        ]
    );

    $name = $request['name'];
    $role = new Role();
    $role->name = $name;

    $permissions = $request['permissions'];

    $role->save();
    
    foreach ($permissions as $permission) {
        $p = Permission::where('id', '=', $permission)->firstOrFail(); 
        $role = Role::where('name', '=', $name)->first(); 
        $role->givePermissionTo($p);
    }
    Alert('','Berhasil Simpan Data','success');
    return redirect('admin/'.$this->url);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    $status = $this->url;
    $data = Role::findOrFail($id);
    return $data->permissions;
    if (empty($data)) {
      return redirect('errors');
    }
    $nametag = $this->nametag;
    return view('view.admin.'.$this->url.'.show', compact('status','data','nametag'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $permissions = Permission::all();
    $status = $this->url;
    $data = $this->model->find($id);
    if (empty($data)) {
      return redirect('errors');
    }
    $nametag = $this->nametag;
    return view('view.admin.'.$this->url.'.edit', compact('status','data','nametag', 'permissions'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $status = $this->url;
    $data = $this->model->find($id);
    $role = Role::findOrFail($id);
        
    $this->validate($request, [
        'name'=>'required|max:10|unique:roles,name,'.$id,
        'permissions' =>'required',
    ]);

    $input = $request->except(['permissions']);
    $permissions = $request['permissions'];
    $role->fill($input)->save();

    $p_all = Permission::all();

    foreach ($p_all as $p) {
        $role->revokePermissionTo($p);
    }

    foreach ($permissions as $permission) {
        $p = Permission::where('id', '=', $permission)->firstOrFail();
        $role->givePermissionTo($p);
    }
    Alert('','Berhasil Ubah Data','success');
    return redirect('admin/'.$this->url);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $status = $this->url;
    $role = Role::findOrFail($id);
    $role->delete();
    Alert('','Berhasil Menghapus Data','success');
    return redirect()->back();
  }
}
