<?php


namespace App\Helpers;
use Request;
use Auth;
use \Carbon\Carbon;
use Spatie\Permission\Models\Role;
use App\Models\modelhasrole;
use Illuminate\Database\Eloquent\Builder;
use App\Models\user;
use App\Models\staticdata;

class SetFunction
{
    public static function regexTimestamp($date){
      $data = preg_replace('/\D/', '', $date);
      return $data;
    }
    public static function listMenu(){
        $datamenu = [
              [
                "name" => ucwords('riwayat'),
                "status" => "single",
                "slug" => "riwayat"
              ],
              [
                "name" => ucwords('bantuan'),
                "status" => "single",
                "slug" => "faq"
              ],
              [
                "name" => ucwords('acara'),
                "status" => "single",
                "slug" => "acara"
              ],
              [
                "name" => ucwords('toko'),
                "status" => "single",
                "slug" => "toko"
              ],
        ];
        return $datamenu;
    }
    public static function ArrayMenu(){
        $data = [
          'faq',
          'berita',
          'acara',
          'toko',
          'riwayat',
          'staticdata',
        ];
        return $data;
    }
    public static function whenRoleHas($model){
      $akses = $role = Auth::user()->roles ? Auth::user()->roles->pluck('name')->implode(' ') : 'member';
      $role = Role::findByName($akses);
      $check = $role->hasPermissionTo($model."-index");
      if ($check) {
        return true;
      }else{
        return false;
      }
    }
    public static function setDateID($date){
      $bulan = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September', 
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
      ];
      $dateparse = Carbon::parse($date);
      $convert = $dateparse->day.' '.$bulan[$dateparse->month].' '.$dateparse->year;
      return $convert;
    }
    public static function staticData($id, $status = "image")
    {
      $data = staticdata::with('file')->where('slug' ,$id)->first();
      $send = '';
      if ($status == 'file') {
        if(!isset($data)) return asset('notfound.png');
        $send = $data->file->count() > 0 ? asset('uploads/'.$data->file->first()->filename) : asset('notfound.png');
      }elseif($status == 'html'){
        $send = $data->detail;
      }elseif($status == 'array'){
        $send = $data;
      }else{
        $send = strip_tags($data->detail);
      }
      return $send;
    }
}