<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\file;
use App\Traits\globalTraits;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;
    // call global function
    use globalTraits;

  
    // Title Page
    static public function nametag(){
        return ucwords('data user');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','noktp','alamat',
        'nohp', 'slug', 'jeniskelamin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // register here, any models have relation with this model will delete when data form this model removed.
    // e.g ['user'=>'hasMany', 'user'=>'hasOne']
    static public function arrayRelationDeleted() {
        $data = [];
        return $data;
    }

    // Rules Attribut
    static public function rules($id=NULL)
    {
        // $fileRequeired = $id == NULL ? 'required' : 'nullable';
        return [
            'name'=>'required',
            'alamat'=>'required',
            'jeniskelamin'=>'required',
            'email'=>'required|string|email|max:255|unique:users,email,'.$id,
            'noktp'=>'nullable|string|max:255|unique:users,noktp,'.$id,
            'nohp'=>'required|string|max:255|unique:users,nohp,'.$id,
            'password'=>'required|string|min:6|confirmed',
            'foto' => 'nullable|mimes:jpeg,bmp,png,PNG,JPG,webp'
        ];
    }

    // Custom Message
    static public function customMessage($id=NULL)
    {
        return [
            'required' => ucwords('Harap isi :attribute'),
            'mimes' => ucwords(':attribute harus berekstensi jpeg,bmp,png,PNG,JPG,webp'),
            'confirmed' => 'Password harus sama',
            'email' => 'Mohon Masukan Email Yang Valid',
            'unique' => 'Data sudah terdaftar'
        ];
    }
    // Relation With File
    public function file(){
        return $this->morphMany('App\Models\file','file');
    }
}
