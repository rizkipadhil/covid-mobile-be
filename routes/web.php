<?php

use Intervention\Image\ImageManagerStatic as Image;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('view.public.index');
});
// Auth::routes();
// Authentication Routes...
// test push notif
// Route::post('message', 'Admin\publicController@setRedis');
// end
Route::get('auth/download/excel', 'API\ApiController@downloadExcel');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Auth::routes([
//   'register' => false
// ]);

Route::get('home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::get('admin/tatacara', function(){
      return view('panduan.tatacara');
    });
    Route::get('publish/{model}/{id}/{status}', 'Admin\publicController@publishData');
    
    // store file
    Route::post('store/file/{id}/{model}/{namearray}', 'Admin\publicController@storeFile');

    // add ?json=true for response json
    Route::get('remove/file/{id}', 'Admin\publicController@deleteFile');
    Route::get('admin/tracking', 'Admin\publicController@tracking');
    Route::get('admin/tracking/result', 'Admin\publicController@trackresult');
    Route::get('publish/{model}/{id}/{status}', 'Admin\publicController@publishData');
    Route::get('settings', 'Admin\publicController@settings')->name('index.setting');
    Route::post('settings/changepassword', 'Admin\publicController@changePassword')->name('index.changepassword');
    Route::post('settings/profile', 'Admin\publicController@changeProfil')->name('index.profil');
    Route::group(['namespace'=>'Admin\View','prefix'=>'admin'], function(){
        $data = \SetFunction::ArrayMenu();
        foreach ($data as $item) {
          Route::resource($item, $item.'Controller');
        }
        Route::resource('file', 'fileController');
        Route::resource('permission', 'PermissionController');
        Route::resource('role', 'RoleController');
        Route::resource('user', 'userController');
    });
});