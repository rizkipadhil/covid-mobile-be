@extends('view.admin.layouts.app')
@section('tagmenu')
   Setting User
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12" style="margin-bottom: 30px;">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 pdless">
								<a href="{{ \SetFunction::staticData('tatacara', 'file') }}" class="btn btn-outline-success btn-sm" download target="_blank">Download Panduan</a>
								<hr>
								<iframe src="{{ \SetFunction::staticData('tatacara', 'file') }}" style="width: 100%; height: 650px;" frameborder="0"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
