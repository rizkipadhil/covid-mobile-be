<div class="createImage">
    <h5 style="font-size:14px;"><span class="fa fa-plus"></span> Upload File (Ukuran Maks. 5 Mb)  </h5>
    <hr>
    <div class="col-md-12" id="formAddImage">
      <form class="dropzone" id="dropzone{{$file}}" action="{{url('store/file/'.$data->id.'/'.$status.'/file')}}" method="POST">
        @csrf
      </form>
    </div>
</div>
@section('style')
  <link rel="stylesheet" href="{{ asset('externalasset/dropzone.css') }}">
@endsection
@section('scripts')  
  <script src="{{ asset('externalasset/dropzone.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
      Dropzone.autoDiscover = false;
      var request = new XMLHttpRequest();
      var myDropzone{{$file}} = new Dropzone("#dropzone{{$file}}", {
          paramName: "{{ $file }}",
          maxFilesize: 10000,
          parallelUploads : 1,
          acceptedFiles: ".png, .jpg,.gif, .jpeg, .PNG, .JPG",
          dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg" style="font-size: 14px;"><i class="fa fa-caret-right text-danger"></i> Seret file kedalam Kotak <span class="font-xs">Untuk Upload</span></span><span>&nbsp&nbsp<h4 class="display-inline" style="font-size: 14px;"> (Atau Klik)</h4></span>',
          dictResponseError: 'Error Saat Upload file!'
      });
      $(document).ready(function() {
          myDropzone{{$file}}.on("queuecomplete", function(file) {
              window.location.reload();
          });
      });
  </script>
@endsection