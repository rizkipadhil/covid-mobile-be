<table>
    <thead>
        <tr>
            <th style="border: 1px solid black; border-collapse: collapse;" width="10">{{ '#' }}</th>
            <th style="border: 1px solid black; border-collapse: collapse;" width="40">Email</th>
            <th style="border: 1px solid black; border-collapse: collapse;" width="40">Created At</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @forelse ($data as $item)
            <tr>
                <td style="border: 1px solid black; border-collapse: collapse;">{{ $no++ }}</td>
                <td style="border: 1px solid black; border-collapse: collapse;">{{ $item->email }}</td>
                <td style="border: 1px solid black; border-collapse: collapse;">{{ $item->created_at }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="3" style="border: 1px solid black; border-collapse: collapse;">Empty Data ...</td>
            </tr>
        @endforelse
    </tbody>
</table>