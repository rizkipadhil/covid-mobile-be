@php
    $option = [];
    $option['class'] = 'form-control';
    if (isset($placeholder)){
        $option['placeholder'] = $placeholder;
    }
@endphp
@if ($form == 'text')
{!! Form::text($name, null, $option) !!}
@elseif ($form == 'default')
@if ($type == 'hidden')
    {!! Form::hidden($name, $default, $option) !!}
@else
    {!! Form::text($name, $default, $option) !!}
@endif
@elseif ($form == 'textarea')
{!! Form::textarea($name, null, $option) !!}
@elseif ($form == 'editor')
@php
    $option['id'] = $name.'editor';
@endphp
{!! Form::textarea($name, null, $option) !!}
@elseif ($form == 'number')
{!! Form::number($name, null, $option) !!}
@elseif ($form == 'date')
{!! Form::date($name, null, $option) !!}
@elseif ($form == 'time')
{!! Form::time($name, null, $option) !!}
@elseif ($form == 'file')
@php
    if (isset($accept)){
        $option['accept'] = $accept;
    }
@endphp
{!! Form::file($name, $option) !!}
@elseif ($form == 'select')
{!! Form::select($name, $dataselect, null,$option) !!}
@else
{{ 'Data Form Tidak Ada' }}
@endif