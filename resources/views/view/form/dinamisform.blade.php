@php
    $editor = [];
@endphp
@foreach ($form as $item)
  @php
      $option = [];
      $option = $item;
      if($option['form'] == 'editor'){
        $editor[]['name'] = $item['name'].'editor'; 
      }
      if (isset($item['asset'])){
        $option['asset'] = $item['asset'];
      }
      if (isset($item['data'])){
        $option['dataselect'] = $item['data'];
      }
      if (isset($item['placeholder'])){
        $option['placeholder'] = ucwords($item['placeholder']);
      }
  @endphp
  <div class="row">
    <div class="col-md-12">
      <div class="form-group label-floating {{ $errors->has($item['name']) ? 'has-error' :'' }}">
        @if (isset($option['type']))
          @if ($option['type'] !== 'hidden')
            <label class="control-label cap">{{ ucwords($item['title']) }}</label>
          @endif
        @else
          <label class="control-label cap">{{ ucwords($item['title']) }}</label>
        @endif
        @include('view.form.form', $option)
        @if ($errors->has($item['name']))
          <span class="text-muted" style="color:red;">{{$errors->first($item['name'])}}</span>
        @endif
      </div>
    </div>
  </div>
@endforeach
@if (count($editor) > 0)
  @section('dropzone')
    <link href="{{ asset('externalasset/summernote/summernote.css') }}" rel="stylesheet">
    <script src="{{ asset('externalasset/summernote/summernote.js') }}"></script>
  @endsection
      
  @section('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        @foreach ($editor as $itemeditor)
          $('#{{$itemeditor['name']}}').summernote({
              tabsize: 2,
              popover: {
                  image: [],
                  link: [],
                  air: []
              },
              height: 500
          });
        @endforeach
      });
    </script>
  @endsection
@endif