{{-- if u want to use Dinamis Form, see the intruction on readme. --}}
@php
    $option = [];
    $option['form'] = $form;
    if (isset($data))
        $option['data'] = $data;
@endphp
@include('view.form.dinamisform', $option)