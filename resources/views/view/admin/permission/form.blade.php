<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('name') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('name') }}</label>
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('name'))
        <span class="text-muted" style="color:red;">{{$errors->first('name')}}</span>
      @endif
    </div>
  </div>
</div>