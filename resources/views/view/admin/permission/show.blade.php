@extends('view.admin.layouts.app')
@section('tagmenu')
    <a href="{{ route($status.'.index') }}" class="btn btn-success"><b> <span class="fas fa-chevron-left"></span> Kembali</b></a>
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title-1" style="margin-bottom:20px;">Detail {{ ucwords($nametag) }}<h2>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-title" style="padding-top: 30px;">
                        <div class="container-title" style="padding: 0px 20px;">
                        <div class="title-page" style="font-size: 20px;font-weight: bold;">
                            @yield('tagmenu')
                        </div>
                        <hr>
                        <div class="button-list">
                            @yield('button')
                        </div>
                        </div>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
				         		<hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3>{{ $data->name }}</h3>
                                        <span class="text-muted" style="font-size:12px;opacity:0.8;">Diterbitkan pada {{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</span>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('scripts')

@endsection
