@php
$no = 1;
@endphp
<table class="table" id="tabledata">
    <thead>
        <tr>
            <th>No</th>
            <th>Aksi</th>
            <th>Nama / NIK</th>
            <th>
                Tanggal
            </th>
            <th>Lokasi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($datas as $data)
        @php
            $callData = $data->keterangan == 'user' ? 'name' : 'nama';
        @endphp
        <tr>
            <td>{{ $no++ }}</td>
            <td>
                @if ($data->keterangan == 'user')
                    <span class="alert alert-sm alert-info">Bertemu</span>
                @elseif ($data->keterangan == 'toko')
                    <span class="alert alert-sm alert-success">Mengunjungi</span>
                @else
                    <span class="alert alert-sm alert-warning">Menghadiri</span>                
                @endif
            </td>
            <td>
                @if ($data->keterangan == 'user')
                    @php
                        $noktp = $data->keterangan == 'user' ? '/ '.$data->riwayat->noktp : '';
                    @endphp
                    <a href="{{ url('admin/tracking/result?nik='.$data->riwayat->noktp.'&tanggalawal='.$request['tanggalawal'].'&tanggalakhir='.$request['tanggalakhir']) }}">
                        {{ isset($data->riwayat) ? ucwords($data->riwayat[$callData]) : '-' }} {{ $noktp }}
                    </a>
                @else
                    {{ isset($data->riwayat) ? ucwords($data->riwayat[$callData]) : '-' }}
                @endif
            </td>
            <td>{{ \Carbon\Carbon::parse($data->created_at) }}</td>
            <td>
                <a href="https://maps.google.com/?q={{$data->lat}},{{$data->lang}}" target="_blank" class="btn btn-sm btn-outline-danger">Lokasi</a>
            </td>
        </tr>
        @empty
        <tr colspan='5'>
            <td>Data Kosong</td>
        </tr>
        @endforelse
    </tbody>
</table>