@extends('view.admin.layouts.app')
@section('tagmenu')
    {{ucwords('Result Tracking Pengguna')}} 
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
          <div class="card-title" style="padding-top: 30px;">
              <div class="container-title" style="padding: 0px 20px;">
              <div class="title-page" style="font-size: 20px;font-weight: bold;">
                  @yield('tagmenu')
              </div>
              <hr>
              <div class="button-list">
                  @yield('button')
              </div>
              </div>
          </div>
					<div class="card-body">

            <table class="table">
              <thead>
                <tr>
                  <th colspan="2">
                    Detail Tracking
                  </th>
                </tr>
                <tr>
                  <th>
                    NIk
                  </th>
                  <th>
                    {{ $request['nik'] }}
                  </th>
                </tr>
                <tr>
                  <th>
                    Tanggal Mulai
                  </th>
                  <th>
                    {{ $request['tanggalawal'] }}
                  </th>
                </tr>
                <tr>
                  <th>
                    Tanggal Akhir
                  </th>
                  <th>
                    {{ $request['tanggalakhir'] }}
                  </th>
                </tr>
              </thead>
            </table>
						<div class="row">
							<div class="col-lg-12 table-responsive">
                @include('view.admin.track.table', ['datas' => $datas, 'request' => $request])
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('styles')
  <link rel="stylesheet" href="{{ asset('externalasset/dataTables.bootstrap4.min.css') }}">
@endsection
@section('scripts')
  <script src="{{ asset('externalasset/jquery.dataTables.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('externalasset/dataTables.bootstrap4.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#tabledata').DataTable();
    } );
  </script>
@endsection
