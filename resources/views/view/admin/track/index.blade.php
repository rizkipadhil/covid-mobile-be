@extends('view.admin.layouts.app')
@section('tagmenu')
    {{ucwords('Tracking Pengguna')}} 
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
          <div class="card-title" style="padding-top: 30px;">
              <div class="container-title" style="padding: 0px 20px;">
              <div class="title-page" style="font-size: 20px;font-weight: bold;">
                  @yield('tagmenu')
              </div>
              <hr>
              <div class="button-list">
                  @yield('button')
              </div>
              </div>
          </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 table-responsive">
                <form action="{{ url('admin/tracking/result') }}">
                  {{--  startform  --}}
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group label-floating {{ $errors->has('nik') ? 'has-error' :'' }}">
                        <label class="control-label cap">{{ ucwords('NIK') }}</label>
                        {!! Form::text('nik', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('nik'))
                          <span class="text-muted" style="color:red;">{{$errors->first('nik')}}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group label-floating {{ $errors->has('tanggalawal') ? 'has-error' :'' }}">
                        <label class="control-label cap">{{ ucwords('tanggal Mulai') }}</label>
                        {!! Form::date('tanggalawal', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('tanggalawal'))
                          <span class="text-muted" style="color:red;">{{$errors->first('tanggalawal')}}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group label-floating {{ $errors->has('tanggalakhir') ? 'has-error' :'' }}">
                        <label class="control-label cap">{{ ucwords('tanggal akhir') }}</label>
                        {!! Form::date('tanggalakhir', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('tanggalakhir'))
                          <span class="text-muted" style="color:red;">{{$errors->first('tanggalakhir')}}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  {{--  endform  --}}
                  <button type="submit" class="btn btn-primary pull-right cap">Generate</button>
                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('styles')
  <link rel="stylesheet" href="{{ asset('externalasset/dataTables.bootstrap4.min.css') }}">
@endsection
@section('scripts')
  <script src="{{ asset('externalasset/jquery.dataTables.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('externalasset/dataTables.bootstrap4.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#tabledata').DataTable();
    } );
  </script>
@endsection
