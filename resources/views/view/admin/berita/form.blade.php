<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('judul') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('Judul') }}</label>
      {!! Form::text('judul', null, ['class' => 'form-control']) !!}
      @if ($errors->has('judul'))
        <span class="text-muted" style="color:red;">{{$errors->first('judul')}}</span>
      @endif
    </div>
  </div>
</div>
{{-- text editor --}}
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('isi') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('Isi') }}</label>
      {!! Form::textarea('isi', null, ['class' => 'form-control','id'=>'summernote']) !!}
      @if ($errors->has('isi'))
        <span class="text-muted" style="color:red;">{{$errors->first('isi')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Foto</label>
      <div class="form-group label-floating  {{ $errors->has('foto') ? 'has-error' :'' }}">
        {!! Form::file('foto', ['class' => 'form-control','accept'=>'image/x-png,image/gif,image/jpeg']) !!}
        @if ($errors->has('foto'))
          <span class="text-muted">{{$errors->first('foto')}}</span>
        @endif
      </div>
    </div>
  </div>
</div>
<!-- include summernote css/js -->
@section('scripts')
  <script src="{{ asset('externalasset/summernote/summernote.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#summernote').summernote({
      tabsize: 2,
      popover: {
        image: [],
        link: [],
        air: []
      },
      height: 500
    });
  });
  </script>
@endsection
@section('style')
  <link href="{{ asset('externalasset/summernote/summernote.css') }}" rel="stylesheet">
@endsection
