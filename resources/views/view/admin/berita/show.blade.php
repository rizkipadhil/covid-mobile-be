@extends('view.admin.layouts.app')
@section('tagmenu')
    <a href="{{ route($status.'.index') }}" class="btn btn-success"><b> <span class="fas fa-chevron-left"></span> Kembali</b></a>
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-title" style="padding-top: 30px;">
                        <div class="container-title" style="padding: 0px 20px;">
                            <div class="title-page" style="font-size: 20px;font-weight: bold;">
                                <h2 class="title-1" style="margin-bottom:20px;">Detail {{ ucwords($nametag) }}<h2>
                                @yield('tagmenu')
                            </div>
                            <hr>
                            <div class="button-list">
                                @yield('button')
                            </div>
                        </div>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
				         		<hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3>{{ $data->judul }}</h3>
                                        <span class="text-muted" style="font-size:12px;opacity:0.8;">Diterbitkan pada {{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</span>
                                    </div>
                                    <div class="col-md-6" style="margin-top: 30px;">
                                        <div class="text-right">
                                            @if ($data->publish == 'inactive')
                                                <a class="btn btn-sm btn-outline-success" href="{{url('publish/'.$status.'/'.$data->id.'/active')}}"><span class="fa fa-check-square"></span> Publikasikan</a>
                                            @else
                                                <a class="btn btn-sm btn-outline-danger"  onclick="return confirm('Apakah anda yakin?')" href="{{url('publish/'.$status.'/'.$data->id.'/inactive')}}"><span class="fa fa-minus-square"></span> Batalkan Publikasi</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <!-- Nav tabs -->
                                <div class="text-center">
                                    <img src="{{ $data->file->count() > 0 ? asset('uploads/'.$data->file->first()->filename) : asset('notfound.png') }}" style="width:30%" alt="">
                                </div>
                                <hr>
                                {!! $data->isi !!}
                                <hr>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Detail Lain</th>
                                        </tr>
                                        <tr>
                                            <th>Dibuat</th>
                                            <th>{{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</th>
                                        </tr>
                                        <tr>
                                            <th>Terakhir Update</th>
                                            <th>{{ \Carbon\Carbon::parse($data->updated_at)->toFormattedDateString() }}</th>
                                        </tr>
                                    </thead>
                                </table>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('scripts')

@endsection
