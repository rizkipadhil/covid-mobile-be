<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('name') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('name') }}</label>
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('name'))
        <span class="text-muted" style="color:red;">{{$errors->first('name')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="col-md-6">
      <label class="control-label cap">{{ ucwords('permissions') }}</label>
    </div>
    <div class="col-md-6">
      <div class="form-group label-floating {{ $errors->has('permissions') ? 'has-error' :'' }}">
        @foreach ($permissions as $permission)
        {{ Form::checkbox('permissions[]',  $permission->id, isset($role) ? $role->permissions : null ) }}
        {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
        @endforeach
      </div>
    </div>
  </div>
</div>