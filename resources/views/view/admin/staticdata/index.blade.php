@extends('view.admin.layouts.app')
@section('tagmenu')
    {{ucwords($nametag)}} 
@endsection
@section('button')
    @can($status.'-create')
        @hasanyrole('superadmin')
            <a href="{{ route($status.'.create') }}" class="btn btn-success"><b> <span class="fas fa-plus-circle"></span> Buat Data</b></a>
        @else
            @role('member')
                @if (\SetFunction::checkLastCreatedStatus($status, Auth::user()->id))
                    <a href="{{ route($status.'.create') }}" class="btn btn-success"><b> <span class="fas fa-plus-circle"></span> Buat Data</b></a>
                @endif
            @endrole
        @endhasrole
    @endhasanyrole
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-title" style="padding-top: 30px;">
                        <div class="container-title" style="padding: 0px 20px;">
                        <div class="title-page" style="font-size: 20px;font-weight: bold;">
                            @yield('tagmenu')
                        </div>
                        <hr>
                        <div class="button-list">
                            @yield('button')
                        </div>
                        </div>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 table-responsive">
        						@php
                                $no = 1;
                                @endphp
                                <table class="table" id="tabledata">
                                    <thead>
                                        <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>slug</th>
                                        <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($datas as $data)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->slug }}</td>
                                            <td>
                                                @can($status.'-update')
                                                    <a href="{{ route($status.'.edit',[$data->id]) }}" class="btn btn-sm btn-outline-warning" style="width:150px;margin-top:5px;">Ubah</a>
                                                @endcan
                                                @can($status.'-delete')
                                                    {!! Form::open(['route' => [$status.'.destroy', $data->id], 'method' => 'delete']) !!}
                                                        {!! Form::button('Hapus Data', ['type' => 'submit', 'class' => 'btn btn-sm btn-outline-danger', 'style'=>'width:150px;margin-top:7px;', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                        @empty
                                        <tr colspan='5'>
                                            <td>Data Kosong</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('styles')
  <link rel="stylesheet" href="{{ asset('externalasset/dataTables.bootstrap4.min.css') }}">
@endsection
@section('scripts')
  <script src="{{ asset('externalasset/jquery.dataTables.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('externalasset/dataTables.bootstrap4.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#tabledata').DataTable();
    } );
  </script>
@endsection
