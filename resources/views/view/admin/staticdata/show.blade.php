@extends('view.admin.layouts.app')
@section('tagmenu')
    <a href="{{ route($status.'.index') }}" class="btn btn-success"><b> <span class="fas fa-chevron-left"></span> Kembali</b></a>
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-title" style="padding-top: 30px;">
                        <div class="container-title" style="padding: 0px 20px;">
                            <div class="title-page" style="font-size: 20px;font-weight: bold;">
                                <h2 class="title-1" style="margin-bottom:20px;">Detail {{ ucwords($nametag) }}<h2>
                                @yield('tagmenu')
                            </div>
                            <hr>
                            <div class="button-list">
                                @yield('button')
                            </div>
                        </div>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
				         		<hr>
                                <div class="row">
                                    <div class="col-md-9">
                                        <h3>Pengajuan Oleh {{ ucwords($data->user->name) }} </h3>
                                        <span class="text-muted" style="font-size:12px;opacity:0.8;"> 
                                            Dengan No Pendaftaran 
                                            {{ $data->no_pendaftaran !== NULL ? \SetFunction::getCodeModel($status).'/'.\SetFunction::generateTahun($data->created_at).'/'.$data->no_pendaftaran : '-'  }} 
                                            <br> 
                                            Diterbitkan pada {{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</span>
                                        <span class="text-muted" style="font-size:12px;opacity:0.8;">
                                            <br> No Handphone: {{ $data->user->nohp }} <br>
                                            Alamat Email: {{ $data->user->noktp }}
                                        </span>
                                    </div>
                                    <div class="col-md-3" style="margin-top: 30px;">
                                        <div class="text-right">
                                            @hasanyrole('member')
                                                @if ($data->status == 'input')
                                                    {{ 'Proses Input Data Oleh User' }} <br>
                                                    <span class="text-muted" style="font-size: 14px;">
                                                        {{ '*Setelah Data Selesai Terinput Semua Akan Tampil Tombol Ajukan Data' }}
                                                    </span>
                                                @elseif ($data->status == 'tolak')
                                                    <span style="color:red;">
                                                        Data Ditolak
                                                    </span> <br>
                                                    Alasan : {{ $data->keterangantolak }}
                                                @elseif ($data->status == 'proses')
                                                    <span>
                                                        Data Masih Dalam Proses
                                                    </span>
                                                @else
                                                    <span style="color:green;">
                                                        Data Diterima
                                                    </span> <br>
                                                    Keterangan : {{ $data->keterangantolak }}
                                                @endif
                                            @else
                                                @if ($data->status == 'input')
                                                    {{ 'Proses Input Data Oleh User' }} <br>
                                                    <span class="text-muted" style="font-size: 14px;">
                                                        {{ '*Setelah Data Selesai Terinput Semua Akan Tampil Tombol Ajukan Data' }}
                                                    </span>
                                                @elseif ($data->status == 'proses')
                                                    <a class="btn btn-sm btn-success" onclick="terimaData('{{$data->id}}','{{$status}}')"><span class="fa fa-check-square"></span> Terima</a>
                                                    <a class="btn btn-sm btn-danger"  onclick="tolakData('{{$data->id}}','{{$status}}')"><span class="fa fa-minus-square"></span> Tolak</a>
                                                @elseif ($data->status == 'tolak')
                                                    <span style="color:red;">
                                                        Data Ditolak
                                                    </span> <br>
                                                    Alasan : {{ $data->keterangantolak }}
                                                @else
                                                    <span style="color:green;">
                                                        Data Diterima
                                                    </span> <br>
                                                    Keterangan : {{ $data->keterangantolak }}
                                                @endif
                                            @endhasanyrole
                                        </div>
                                    </div>
                                </div>
                                <!-- Nav tabs -->
                                <div class="row" style="margin-top: 30px; margin-bottom: 30px;">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Riwayat Pengajuan</th>
                                                </tr>
                                                @foreach ($data->riwayat as $riwayat)
                                                    <tr>
                                                        <th>
                                                            Status Pengajuan: 
                                                            @if ($riwayat->status == 'input')
                                                                {{ 'Proses Input Data Oleh User' }} 
                                                            @elseif ($riwayat->status == 'proses')
                                                                {{ 'Data Sedang Di Proses' }}
                                                            @elseif ($riwayat->status == 'tolak')
                                                                <span style="color:red;">
                                                                    Data Ditolak
                                                                </span>
                                                            @else
                                                                <span style="color:green;">
                                                                    Data Diterima @if($riwayat->file->count() > 0) | <a href="{{ $riwayat->file->count() > 0 ? asset('uploads/'.$riwayat->file->first()->filename) : asset('notfound.png') }}" target="_blank">Download Bukti Pembayaran</a> @endif
                                                                </span>
                                                            @endif
                                                            <br>
                                                            Pesan: {{ ucwords($riwayat->message) }}
                                                        </th>
                                                        <th>
                                                            {{ \Carbon\Carbon::parse($riwayat->created_at)->format('d-m-Y H:i:s') }}
                                                        </th>
                                                    </tr>
                                                @endforeach
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Unduh Persyaratan Berkas</th>
                                                </tr>
                                                @foreach (\SetFunction::getBerkas($status) as $berkas)
                                                    <tr>
                                                        <th>
                                                            {{$berkas->name}}
                                                        </th>
                                                        <th>
                                                            <a href="{{ $berkas->file->count() > 0 ? asset('uploads/'.$berkas->file->first()->filename) : asset('notfound.png') }}" target="_blank">Download Berkas</a>
                                                        </th>
                                                    </tr>
                                                @endforeach
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Persyaratan</th>
                                                </tr>
                                                @foreach (\SetFunction::getListPersyaratan($status) as $keyper => $persyaratan)
                                                    <tr>
                                                        <th colspan="2">
                                                            <h5>{{ $persyaratan }}
                                                            @hasanyrole('member|superadmin')
                                                                @if ($data->{$keyper} !== '' && $data->{$keyper} !== NULL)
                                                                    <a class="btn btn-sm btn-outline-primary" href="{{ asset('uploads/'.$data->{$keyper}) }}" target="_blank">Lihat File Persyaratan</a> <br>
                                                                    <span class="text-muted" style="font-size: 12px;">
                                                                        * Jika ingin mengganti file silahkan upload kembali data!
                                                                    </span>
                                                                @endif
                                                                <br>
                                                                @if (\SetFunction::checkDataType($status)[$keyper] == 'required')
                                                                    <span class="text-muted" style="font-size: 12px; color:red !important;">
                                                                        * Data Wajib Diisi
                                                                    </span>
                                                                @endif
                                                                </h5>
                                                                @if ($data->status !== 'proses' && $data->status !== 'terima')
                                                                    @include('global.multiupload', ['file'=>$keyper])
                                                                @endif
                                                            @else
                                                                @if ($data->{$keyper} !== '' && $data->{$keyper} !== NULL)
                                                                    <a class="btn btn-sm btn-outline-primary" href="{{ asset('uploads/'.$data->{$keyper}) }}" target="_blank">Lihat File Persyaratan</a> <br>
                                                                    @else
                                                                    <a class="btn btn-sm btn-outline-warning" target="_blank">
                                                                        Data Belum Diupload
                                                                    </a>
                                                                    <br>
                                                                @endif
                                                                </h5>
                                                            @endhasanyrole
                                                        </th>
                                                    </tr>
                                                @endforeach
                                                @php
                                                    $filependukung = \SetFunction::checkFilePendukung($status);
                                                @endphp
                                                @if ($filependukung['status'] == 'ada')
                                                    @php
                                                        $getfile = \App\Models\file::where('filename', 'LIKE', '%file_pendukung%')->where('file_type', get_class($data))->where('file_id', $data->id);
                                                    @endphp
                                                    <tr>
                                                        <th colspan="2">
                                                            <h5>{{ $filependukung['message'] }}
                                                            @hasanyrole('member|superadmin')
                                                                @if ($getfile->count() > 0 && $getfile->count() > 0)
                                                                    <a class="btn btn-sm btn-outline-primary" href="{{ asset('uploads/'.$getfile->first()->filename ) }}" target="_blank">Lihat File Persyaratan</a> <br>
                                                                    <span class="text-muted" style="font-size: 12px;">
                                                                        * Jika ingin mengganti file silahkan upload kembali data!
                                                                    </span>
                                                                @endif
                                                                </h5>
                                                                @if ($data->status !== 'proses' && $data->status !== 'terima')
                                                                    @include('global.multiupload', ['file'=>'file_pendukung'])
                                                                @endif
                                                            @else
                                                                @if ($getfile->count() > 0 && $getfile->count() > 0)
                                                                    <a class="btn btn-sm btn-outline-primary" href="{{ asset('uploads/'.$getfile->first()->filename ) }}" target="_blank">Lihat File Persyaratan</a> <br>
                                                                @else
                                                                    <a class="btn btn-sm btn-outline-warning" target="_blank">
                                                                        Data Belum Diupload
                                                                    </a>
                                                                    <br>
                                                                @endif
                                                                </h5>
                                                            @endhasanyrole
                                                        </th>
                                                    </tr>
                                                @endif
                                            </thead>
                                        </table>
                                    </div>
                                    @hasanyrole('member|superadmin')
                                        @if ($data->status !== 'proses' && $data->status !== 'terima')
                                            @if (\SetFunction::checkAjukanBerkas($data->id, $status))
                                                <div class="col-md-12">
                                                    <div class="text-right">
                                                        <a class="btn btn-lg btn-outline-success" href="{{url('publish/'.$status.'/'.$data->id.'/proses')}}"><span class="fa fa-check-square"></span> Ajukan Data</a>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-12">
                                                    <div class="text-right">
                                                        <a class="btn btn-lg btn-outline-success" disabled><span class="fa fa-check-square"></span> Ajukan Data</a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    @endhasanyrole
                                </div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('scripts')
    @foreach (\SetFunction::getListPersyaratan($status) as $keypera => $persyaratana)
        <script type="text/javascript">
            Dropzone.autoDiscover = false;
            var request = new XMLHttpRequest();
            var myDropzone{{$keypera}} = new Dropzone("#dropzone{{$keypera}}", {
                paramName: "{{ $keypera }}",
                maxFilesize: 10000,
                parallelUploads : 1,
                maxFiles: 1,
                acceptedFiles: ".pdf, .PDF",
                dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg" style="font-size: 14px;"><i class="fa fa-caret-right text-danger"></i> Seret file kedalam Kotak <span class="font-xs">Untuk Upload</span></span><span>&nbsp&nbsp<h4 class="display-inline" style="font-size: 14px;"> (Atau Klik)</h4></span>',
                dictResponseError: 'Error Saat Upload file!'
            });
            $(document).ready(function() {
                myDropzone{{$keypera}}.on("queuecomplete", function(file) {
                    window.location.reload();
                });
            });
        </script>
    @endforeach
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var request = new XMLHttpRequest();
        var myDropzone{{'file_pendukung'}} = new Dropzone("#dropzone{{'file_pendukung'}}", {
            paramName: "{{ 'file_pendukung' }}",
            maxFilesize: 10000,
            parallelUploads : 1,
            maxFiles: 1,
            acceptedFiles: ".pdf, .PDF",
            dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg" style="font-size: 14px;"><i class="fa fa-caret-right text-danger"></i> Seret file kedalam Kotak <span class="font-xs">Untuk Upload</span></span><span>&nbsp&nbsp<h4 class="display-inline" style="font-size: 14px;"> (Atau Klik)</h4></span>',
            dictResponseError: 'Error Saat Upload file!'
        });
        $(document).ready(function() {
            myDropzone{{'file_pendukung'}}.on("queuecomplete", function(file) {
                window.location.reload();
            });
        });
    </script>
@endsection
