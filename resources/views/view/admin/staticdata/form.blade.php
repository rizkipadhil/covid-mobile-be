<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('name') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('name') }}</label>
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('name'))
        <span class="text-muted" style="color:red;">{{$errors->first('name')}}</span>
      @endif
    </div>
  </div>
</div>
@if (isset($data))
@else
  <div class="row">
    <div class="col-md-12">
      <div class="form-group label-floating {{ $errors->has('slug') ? 'has-error' :'' }}">
        <label class="control-label cap">{{ ucwords('slug') }}</label>
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        @if ($errors->has('slug'))
          <span class="text-muted" style="color:red;">{{$errors->first('slug')}}</span>
        @endif
      </div>
    </div>
  </div>
@endif
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Upload Data</label>
      <div class="form-group label-floating  {{ $errors->has('foto') ? 'has-error' :'' }}">
        {!! Form::file('foto', ['class' => 'form-control']) !!}
        @if ($errors->has('foto'))
          <span class="text-muted">{{$errors->first('foto')}}</span>
        @endif
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('detail') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('detail') }}</label>
      {!! Form::textarea('detail', null, ['class' => 'form-control', "rows"=>"30", "cols"=>"550", 'id'=>'summernote']) !!}
      @if ($errors->has('detail'))
        <span class="text-muted" style="color:red;">{{$errors->first('detail')}}</span>
      @endif
    </div>
  </div>
</div>
<!-- include summernote css/js -->
@section('scripts')
  <script src="{{ asset('externalasset/summernote/summernote.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#summernote').summernote({
      tabsize: 2,
      popover: {
        image: [],
        link: [],
        air: []
      },
      height: 500
    });
  });
  </script>
@endsection
@section('style')
  <link href="{{ asset('externalasset/summernote/summernote.css') }}" rel="stylesheet">
@endsection
