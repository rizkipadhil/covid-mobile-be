
  <li class="{{ \Request::has('admin/home') ? 'active' : ''  }}">
    <a href="{{url('home')}}" class="waves-effect">
      <i class="fas fa-home"></i>Dashboard <b></b>
    </a>
  </li>
  <li class="{{ \Request::has('admin/tatacara') ? 'active' : ''  }}">
    <a href="{{url('admin/tatacara')}}" class="waves-effect">
      <i class="fas fa-check-square"></i>Panduan Penggunaan <b></b>
    </a>
  </li>
  @php
  $datamenu = \SetFunction::listMenu();
  @endphp
  @foreach ($datamenu as $key => $menu)
    @if ($menu['status'] == 'triple')
      <li class="menu-title active nav-active">{{ ucwords($menu['name']) }}</li>
      @foreach ($menu['data'] as $sub)
          <li class="
            has_sub
            @foreach($sub['data'] as $multicheck)
              {{ \Request::has('admin/'.$multicheck['slug']) || \Request::has('admin/'.$multicheck['slug'].'/*') ? 'active nav-active' : ''  }}
            @endforeach
          ">
              <a href="javascript:void(0);" class="waves-effect">
                <i class="fas fa-folder" style="width: 10%;"></i> 
                  <span style="flex-grow: 1;">
                  {{ ucwords($sub['name']) }} 
                  <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span>
                </span>
              </a>
              <ul class="list-unstyled">
                @foreach ($sub['data'] as $multisub)
                  @can($sub['slug'].'-index')
                    <li class="{{ \Request::has('admin/'.$multisub['slug']) || \Request::has('admin/'.$multisub['slug'].'/*') ? 'active' : ''  }}"><a href="{{url('admin/'.$multisub['slug'])}}" id="notifside{{$multisub['slug']}}" >{{ ucwords($multisub['name']) }} </a></li>
                  @endcan
                @endforeach
              </ul>
          </li>
      @endforeach
    @elseif ($menu['status'] == 'double')
      <li class="menu-title">{{ ucwords($menu['name']) }}</li>
        @foreach ($menu['data'] as $sub)
          @can($sub['slug'].'-index')
            <li class="{{ \Request::has('admin/'.$sub['slug']) || \Request::has('admin/'.$sub['slug'].'/*') ? 'active' : ''  }}">
              <a href="{{url('admin/'.$sub['slug'])}}" class="waves-effect" style="display:flex;">
                <i class="fas fa-folder" style="width: 10%;"></i> 
                  <span style="flex-grow: 1;" id="notifside{{$sub['slug']}}">
                    {{ ucwords($sub['name'])}} <b></b>
                  </span> 
              </a>
            </li>
          @endcan
        @endforeach
        <li>
          <hr>
        </li>
    @else
      @can($menu['slug'].'-index')
        <li class="{{ \Request::has('admin/'.$menu['slug']) || \Request::has('admin/'.$menu['slug'].'/*') ? 'active' : ''  }}">
          <a href="{{url('admin/'.$menu['slug'])}}" class="waves-effect" style="display:flex;">
            <i class="fas fa-folder" style="width: 10%;"></i> 
              <span style="flex-grow: 1;" id="notifside{{$menu['slug']}}">
                {{ ucwords($menu['name'])}} <b></b>
              </span> 
          </a>
        </li>
      @endcan
    @endif
  @endforeach
  <li>
    <hr>
  </li>
  @role('superadmin')
    <li class="menu-title">Tools</li>
    <li>
      <a href="{{url('admin/tracking')}}" class="waves-effect">
        <i class="fas fa-link"></i>Tracking Pengguna<b></b>
      </a>
    </li>
    <li>
      <a href="{{url('admin/user')}}?role=member" class="waves-effect">
        <i class="fas fa-users"></i>Data User<b></b>
      </a>
    </li>
    <li class="{{ \Request::has('admin/staticdata') || \Request::has('admin/staticdata/*') ? 'active' : ''  }}">
      <a href="{{url('admin/staticdata')}}" class="waves-effect">
        <i class="fas fa-cogs"></i>Static Data <b></b>
      </a>
    </li>
    <li class="{{ \Request::has('admin/role') || \Request::has('admin/role/*') ? 'active' : ''  }}">
      <a href="{{url('admin/role')}}" class="waves-effect">
        <i class="fas fa-cogs"></i>Role <b></b>
      </a>
    </li>
    <li class="{{ \Request::has('admin/permission') || \Request::has('admin/permission/*') ? 'active' : ''  }}">
      <a href="{{url('admin/permission')}}" class="waves-effect">
        <i class="fas fa-cogs"></i>Permission <b></b>
      </a>
    </li>
    <li class="{{ \Request::has('admin/file') || \Request::has('admin/file/*') ? 'active' : ''  }}">
      <a href="{{url('admin/file')}}" class="waves-effect">
        <i class="fas fa-cogs"></i>File <b></b>
      </a>
    </li>
    <li>
      <hr>
    </li>
    <li class="menu-title">User</li>
    <li>
      <a href="{{url('admin/user')}}?role=admin" class="waves-effect">
        <i class="fas fa-users"></i>User Admin<b></b>
      </a>
    </li>
    @endrole
  