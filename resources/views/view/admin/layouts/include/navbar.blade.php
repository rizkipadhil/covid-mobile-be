<ul class="list-inline float-right mb-0">
    <!-- Fullscreen -->
    <li class="list-inline-item dropdown notification-list hidden-xs-down">
        <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
            <i class="mdi mdi-fullscreen noti-icon"></i>
        </a>
    </li>
    <!-- User-->
    <li class="list-inline-item dropdown notification-list">
        <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
            aria-haspopup="false" aria-expanded="false">
            <img src="{{ \App\Models\user::find(Auth::user()->id)->file->count() > 0 ? asset('uploads/'.\App\Models\user::find(Auth::user()->id)->file->first()->filename) : asset('notfound.png') }}" alt="user" class="rounded-circle">
        </a>
        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
            <a class="dropdown-item" href="{{ url('settings') }}"><i class="dripicons-gear text-muted"></i> Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"  href="{{ route('logout') }}">
                  <i class="dripicons-exit text-muted"></i> Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
        </div>
    </li>
</ul>

