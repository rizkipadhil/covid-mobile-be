<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Dawnesia">
    {{--  icon web  --}}
	{{--  <link rel="icon" href="{{asset('uploads/'.callStatikdata(4, 'file'))}}">  --}}
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ \SetFunction::staticData('titleapp') }}</title>

    <!-- C3 charts css -->
    <link href="{{asset('newadmin/plugins/c3/c3.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- Basic Css files -->
    <link href="{{asset('newadmin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('newadmin/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('newadmin/assets/css/style.css')}}" rel="stylesheet" type="text/css">

    <!-- Batch Icons -->
	{{-- <link rel="stylesheet" href="{{asset('folderadmin/files/assets/fonts/batch-icons/css/batch-icons.css')}}"> --}}
    
    <link href="{{asset('newadmin/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('newadmin/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">

    <link rel="stylesheet" href="{{ asset('bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    @yield('style')
    <style>
        .menu-heh-display{
            display: none;
        }
        @media screen and (max-width: 480px){
            .menu-heh-display{
                display: inline;
                margin-top: 20px;
            }
        }
        #titleheha{
            margin-top: 10px;
        }
        #createheha{
            margin-top: 10px;
            content: "\A" !important;
            white-space: pre !important;
        }
        #terimaheha{
            margin-top: 10px;
        }
        #tolakheha{
            margin-top: 10px;
        }
        .colorred{
            color: red !important;
        }
        #listNotifheh{
            height: 300px;
            overflow-y: auto;
        }
        .hidden-heh{
            display: none;
        }
        .pdless
        {
            padding: 0;
        }
        .dataTables_filter {
            float: right !important;
        }
        .form
        {
            display:flex;
        }
        .form-input
        {
            flex-grow: 1;
            padding: 7px 15px;
            border: 0.5px solid #ccc;
            background-color: #f2f2f2;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        .form-btn 
        {
            padding: 7px 20px;
            color: #ffffff;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            background-color: #4272d7;
        }
        .form-btn:hover 
        {
            cursor: pointer;
            transition-duration: 0.5s;
            background-color: #2750a7;
        }
        .text-sm
        {
            font-size: 13px;
            opacity: 0.6;
        }
        .text-md
        {
            padding: 5px;
            background-color: rgb(248, 248, 248);
            padding-left: 10px;
            border-radius: 5px;
            color:black;
            margin-bottom: 5px;
        }
        .img-character
        {
            width: 100px;
            min-height: 150px;
            object-fit:cover;
            margin:auto;
            display:block;
        }
        th:hover
        {
            cursor: ns-resize;
        }
        table *
        {
            font-size: 12px;
        }
    </style>
</head>

<body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="">
                        <!--<a href="index.html" class="logo text-center">Admiria</a>-->
                        <a href="{{ url('/home') }}" class="logo"><img src="{{asset('icon/logoCorona.png')}}" style="width:100%;padding-right:20px;" alt="logo"></a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        <ul>
                            @include('view.admin.layouts.include.sidebar')
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->


            <!-- Start right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            @include('view.admin.layouts.include.navbar')

                            <!-- Page title -->
                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">
                                        {{ \SetFunction::staticData('titleapp') }}
                                    </h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>
                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <!-- ==================
                         PAGE CONTENT START
                         ================== -->

                    <div class="page-content-wrapper">

                        <div class="container-fluid">

                            <div class="row">
                                @yield('content')
                            </div>

                        </div><!-- container -->

                    </div> 
                    <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                    © {{ \Carbon\Carbon::now()->year }} Dawnesia <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Dawnesia</span>
                </footer>

            </div>
            <!-- End Right content here -->
            <!-- Include Push Notif -->
            {{-- @include('global.pushnotif') --}}
            <!-- End Include Push Notif -->
        </div>
        <!-- END wrapper -->


	<script src="https://cdn.jsdelivr.net/npm/vue@2.6.7/dist/vue.js"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    
    <!-- jQuery  -->
    <script src="{{asset('newadmin/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('newadmin/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('newadmin/assets/js/modernizr.min.js')}}"></script>
    <script src="{{asset('newadmin/assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('newadmin/assets/js/waves.js')}}"></script>
    <script src="{{asset('newadmin/assets/js/jquery.nicescroll.js')}}"></script>
    <script src="{{asset('newadmin/assets/js/jquery.scrollTo.min.js')}}"></script>

    <!-- Peity chart JS -->
    <script src="{{asset('newadmin/plugins/peity-chart/jquery.peity.min.js')}}"></script>

    <!--C3 Chart-->
    <script src="{{asset('newadmin/plugins/d3/d3.min.js')}}"></script>
    <script src="{{asset('newadmin/plugins/c3/c3.min.js')}}"></script>

    <!-- KNOB JS -->
    <script src="{{asset('newadmin/plugins/jquery-knob/excanvas.js')}}"></script>
    <script src="{{asset('newadmin/plugins/jquery-knob/jquery.knob.js')}}"></script>

    
    @yield('internalapi')
    
    <!-- Page specific js -->
    <script src="{{asset('newadmin/assets/pages/dashboard.js')}}"></script>
    <!-- App js -->
    <script src="{{asset('newadmin/assets/js/app.js')}}"></script>
    <!-- Main JS-->
	<script src="{{ asset('bower_components/sweetalert2/dist/sweetalert2.min.js') }}" charset="utf-8"></script>
	@yield('vue')
    @yield('dropzone')
	@yield('scripts')
    @yield('extrascripts')
    @yield('modals')
    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    {{-- push notif & other component --}}
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    {{-- end --}}
	@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
</body>

</html>
<!-- end document-->
