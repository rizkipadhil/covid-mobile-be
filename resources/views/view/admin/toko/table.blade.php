@php
$no = 1;
@endphp
<table class="table" id="tabledata">
    <thead>
        <tr>
        <th>No</th>
        <th style="width: 5%;">Qrcode</th>
        <th style="width: 20%;">Pemilik</th>
        <th>Nama</th>
        <th>Keterangan</th>
        <th>Dibuat Pada</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($datas as $data)
        <tr>
            <td>{{ $no++ }}</td>
            <td>
                @php
                    $barcode = [
                        "code" => $data->slug,
                        "status" => 'toko',
                    ];
                    $generate = json_encode($barcode);
                @endphp
                {!! QrCode::size(150)->generate($generate); !!}
            </td>
            {{-- <td>
                <img src="{{ $data->file->count() > 0 ? asset('uploads/'.$data->file->first()->filename) : asset('notfound.png') }}" style="width:100%" alt="">
            </td> --}}
            <td>{{ isset($data->user) ? $data->user->name : '-' }}</td>
            <td>{{ $data->nama }}</td>
            <td>{{ $data->keterangan }}</td>
            <td>{{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</td>
            <td>
                {{-- @can($status.'-view')
                    <a href="{{ route($status.'.show',[$data->id]) }}" class="btn btn-primary" style="width:100px;">Lihat</a> <br>
                @endcan --}}
                @can($status.'-update')
                    <a href="{{ route($status.'.edit',[$data->id]) }}" class="btn btn-warning" style="width:100px;margin-top:5px;">Ubah</a>
                @endcan
                @can($status.'-delete')
                    {!! Form::open(['route' => [$status.'.destroy', $data->id], 'method' => 'delete']) !!}
                    {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger', 'style'=>'width:100px;margin-top:7px;', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    {!! Form::close() !!}
                @endcan
            </td>
        </tr>
        @empty
        <tr colspan='5'>
            <td>Data Kosong</td>
        </tr>
        @endforelse
    </tbody>
</table>