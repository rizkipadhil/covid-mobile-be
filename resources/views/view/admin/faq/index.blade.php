@extends('view.admin.layouts.app')
@section('tagmenu')
    {{ucwords($nametag)}} 
@endsection
@section('button')
    @can($status.'-create')
      <a href="{{ route($status.'.create') }}" class="btn btn-success"><b> <span class="fas fa-plus-circle"></span> Buat Data</b></a>
    @endcan
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-title" style="padding-top: 30px;">
                        <div class="container-title" style="padding: 0px 20px;">
                        <div class="title-page" style="font-size: 20px;font-weight: bold;">
                            @yield('tagmenu')
                        </div>
                        <hr>
                        <div class="button-list">
                            @yield('button')
                        </div>
                        </div>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 table-responsive">
        						@include('view.admin.'.$status.'.table', ['datas' => $datas])
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('styles')
  <link rel="stylesheet" href="{{ asset('externalasset/dataTables.bootstrap4.min.css') }}">
@endsection
@section('scripts')
  <script src="{{ asset('externalasset/jquery.dataTables.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('externalasset/dataTables.bootstrap4.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#tabledata').DataTable();
    } );
  </script>
@endsection
