<input type="hidden" name="redirect" value="{{ $role }}">
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('name') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('nama') }}</label>
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('name'))
        <span style="color:red;">{{$errors->first('name')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('nohp') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('no HP') }}</label>
      {!! Form::text('nohp', null, ['class' => 'form-control']) !!}
      @if ($errors->has('nohp'))
        <span style="color:red;">{{$errors->first('nohp')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('alamat') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('Alamat') }}</label>
      {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
      @if ($errors->has('alamat'))
        <span style="color:red;">{{$errors->first('alamat')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('noktp') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('NIK/No KTP') }}</label>
      {!! Form::text('noktp', null, ['class' => 'form-control']) !!}
      @if ($errors->has('noktp'))
        <span style="color:red;">{{$errors->first('noktp')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('email') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('Email') }}</label>
      {!! Form::email('email', null, ['class' => 'form-control']) !!}
      @if ($errors->has('email'))
        <span style="color:red;">{{$errors->first('email')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('password') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('password') }}</label>
      {!! Form::password('password', ['class' => 'form-control']) !!}
      @if (!empty($data))
        <span style="color:red;">Input hanya jika ingin mengganti password.</span>
      @endif
      @if ($errors->has('password'))
        <span style="color:red;">{{$errors->first('password')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('password_confirmation') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('password confirmation') }}</label>
      {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password-confirm']) !!}
      @if ($errors->has('password_confirmation'))
        <span style="color:red;">{{$errors->first('password_confirmation')}}</span>
      @endif
    </div>
  </div>
</div>
{{-- <div class="row">
  <div class="col-md-12">
    <label class="control-label cap">{{ ucwords('Posisi') }}</label>
    <div class="form-group label-floating {{ $errors->has('roles') ? 'has-error' :'' }}"> --}}
      {{-- <select name="roles[]" class="form-control" required> --}}
        @foreach ($roles as $rol)
          @if ($rol->name !== 'superadmin')
            @if ($rol->name === $_GET['role'])
                <input type="hidden" name="roles[]" value="{{$rol->id}}">
            {{-- <option value="{{$rol->id}}" {{ strtolower($rol->name) == strtolower($role) ? 'selected' : '' }} >{{ ucfirst($rol->name) }}</option> --}}
            @endif
          @endif
          {{-- {{ Form::checkbox('roles[]',  $rol->id, isset($rol) ? $rol->roles : null, ['required'=>'required'] ) }}
          {{ Form::label($rol->name, ucfirst($rol->name)) }}<br> --}}
        @endforeach
      {{-- </select> --}}
    {{-- </div>
  </div>
</div> --}}
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Upload KTP</label>
      <div class="form-group label-floating  {{ $errors->has('foto') ? 'has-error' :'' }}">
        {!! Form::file('foto', ['class' => 'form-control','accept'=>'image/x-png,image/gif,image/jpeg']) !!}
        @if ($errors->has('foto'))
          <span class="text-muted">{{$errors->first('foto')}}</span>
        @endif
      </div>
    </div>
  </div>
</div>
<!-- include summernote css/js -->
@section('scripts')
@endsection
@section('style')
@endsection
