@extends('view.admin.layouts.app')
@section('tagmenu')
    <a href="{{ route($status.'.index') }}?role={{$role}}" class="btn btn-success"><b> <span class="fas fa-chevron-left"></span> Kembali</b></a>
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title-1" style="margin-bottom:20px;">Detail {{ ucwords($nametag) }}<h2>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-title" style="padding-top: 30px;">
                        <div class="container-title" style="padding: 0px 20px;">
                        <div class="title-page" style="font-size: 20px;font-weight: bold;">
                            @yield('tagmenu')
                        </div>
                        <hr>
                        <div class="button-list">
                            @yield('button')
                        </div>
                        </div>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
				         		<hr>
                                <div class="row">
                                <div class="col-md-6">
                                    <h3>{{ $data->name }}</h3>
                                    <span class="text-muted" style="font-size:12px;opacity:0.8;">Diterbitkan pada {{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</span>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                    </div>
                                </div>
                                </div>
                                <hr>
                                <!-- Nav tabs -->
                                <ul class="nav nav-pills nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link {{ isset($_GET['menu']) ? '' : 'active' }}" data-toggle="tab" href="#home">Deksripsi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ isset($_GET['menu']) ? 'active' : '' }}" data-toggle="tab" href="#menu2">Detail lain</a>
                                </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                <div class="tab-pane container {{ isset($_GET['menu']) ? 'fade' : 'active' }}" id="home">
                                        <div class="col-md-12">
                                            <h4>Memiliki Toko</h4>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Toko</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data->toko as $item)
                                                        <tr>
                                                            <td>
                                                                {{$item->nama}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                            <h4>Mengadakan Acara</h4>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Acara</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data->acara as $item)
                                                        <tr>
                                                            <td>
                                                                {{$item->nama}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                            <h4>Riwayat User</h4>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data->riwayat as $item)
                                                        <tr>
                                                            <td>
                                                                {{$item->keterangan}}
                                                                {{-- @if ()
                                                                    
                                                                @endif --}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane container {{ isset($_GET['menu']) ? 'active' : 'fade' }}" id="menu2">
                                    <table class="table">
                                    <thead>
                                        <tr>
                                        <th colspan="3">Detail</th>
                                        </tr>
                                        <tr>
                                        <th>Dibuat</th>
                                        <th>{{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</th>
                                        </tr>
                                        <tr>
                                        <th>Terakhir Update</th>
                                        <th>{{ \Carbon\Carbon::parse($data->updated_at)->toFormattedDateString() }}</th>
                                        </tr>
                                    </thead>
                                    </table>
                                </div>
                                </div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('scripts')

@endsection
