@extends('view.admin.layouts.app')
@section('tagmenu')
    {{ucwords($nametag)}} 
@endsection
@section('button')
    @can($status.'-create')
        @if (Auth::user()->id == 1)
            <a href="{{ route($status.'.create') }}?role={{$role}}" class="btn btn-success"><b> <span class="fas fa-plus-circle"></span> Buat Data</b></a>
        @endif
    @endcan
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="card">
          <div class="card-title" style="padding-top: 30px;">
              <div class="container-title" style="padding: 0px 20px;">
              <div class="title-page" style="font-size: 20px;font-weight: bold;">
                  @yield('tagmenu')
              </div>
              <hr>
              <div class="button-list">
                  @yield('button')
              </div>
              </div>
          </div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 table-responsive">
                @php
                $no = 1;
                @endphp
                <table class="table" id="tabledata">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>QRCODE</th>
                        <th>EMAIL</th>
                        <th>NO KTP/NIK</th>
                        <th>Nama</th>
                        {{-- <th>Role</th> --}}
                        <th>Dibuat Pada</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> 
                        @forelse ($datas as $data)
                            @if (strtolower($data->roles->pluck('name')->implode(' ')) == strtolower($role))
                                @if (strtolower($role) == 'superadmin')
                                    @if ($data->id !== 1)
                                        <tr id="id{{ $data->id }}">
                                            <td>{{ $no++ }}</td>
                                            <td>
                                                @php
                                                    $barcode = [
                                                        "code" => $data->slug,
                                                        "status" => 'user',
                                                    ];
                                                    $generate = json_encode($barcode);
                                                @endphp
                                                {!! QrCode::size(150)->generate($generate); !!}
                                            </td>
                                            <td>{{ $data->email }}</td>
                                            <td>{{ $data->noktp }}</td>
                                            <td>{{ $data->name }}</td>
                                            {{-- <td>{{ $data->roles ? $data->roles->pluck('name')->implode(' ') : '-' }}</td> --}}
                                            <td>{{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</td>
                                            <td>
                                            @if (Auth::user()->id == 1)
                                                @can($status.'-view')
                                                    <a href="{{ route($status.'.show',[$data->id]) }}" class="btn btn-primary" style="width:100px;">Lihat</a> <br>
                                                @endcan
                                                @can($status.'-update')
                                                    <a href="{{ route($status.'.edit',[$data->id]) }}?role={{$role}}" class="btn btn-warning updateheh{{$data->id}}" style="width:100px;margin-top:5px;">Ubah</a>
                                                @endcan
                                                @can($status.'-delete')
                                                    {!! Form::open(['route' => [$status.'.destroy', $data->id], 'method' => 'delete']) !!}
                                                    {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger hapusheh{{$data->id}}', 'style'=>'width:100px;margin-top:7px;', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            @else
                                                User Aktif
                                            @endif
                                            </td>
                                        </tr>
                                    @endif
                                @else
                                    <tr id="id{{ $data->id }}">
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            @php
                                                $barcode = [
                                                    "code" => $data->slug,
                                                    "status" => 'user',
                                                ];
                                                $generate = json_encode($barcode);
                                            @endphp
                                            {!! QrCode::size(150)->generate($generate); !!}
                                        </td>
                                        <td>{{ $data->email }}</td>
                                        <td>{{ $data->noktp }}</td>
                                        <td>{{ $data->name }}</td>
                                        {{-- <td>{{ $data->roles ? $data->roles->pluck('name')->implode(' ') : '-' }}</td> --}}
                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</td>
                                        <td>
                                        @if (Auth::user()->roles->pluck('name')->implode(' ') == 'superadmin' || Auth::user()->roles->pluck('name')->implode(' ') == 'admin')
                                            @can($status.'-view')
                                                <a href="{{ route($status.'.show',[$data->id]) }}" class="btn btn-primary" style="width:100px;">Lihat</a> <br>
                                            @endcan
                                            @can($status.'-update')
                                                <a href="{{ route($status.'.edit',[$data->id]) }}?role={{$role}}" class="btn btn-warning updateheh{{$data->id}}" style="width:100px;margin-top:5px;">Ubah</a>
                                            @endcan
                                            @can($status.'-delete')
                                                {!! Form::open(['route' => [$status.'.destroy', $data->id], 'method' => 'delete']) !!}
                                                {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger hapusheh{{$data->id}}', 'style'=>'width:100px;margin-top:7px;', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                        @else
                                            User Aktif
                                        @endif
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @empty
                        <tr colspan='5'>
                            <td>Data Kosong</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('styles')
  <link rel="stylesheet" href="{{ asset('externalasset/dataTables.bootstrap4.min.css') }}">
@endsection
@section('scripts')
  <script src="{{ asset('externalasset/jquery.dataTables.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('externalasset/dataTables.bootstrap4.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#tabledata').DataTable();
    } );
  </script>
@endsection
