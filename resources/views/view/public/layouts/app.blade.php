<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ \SetFunction::staticData('titleapp') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@200;400;800&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('owl/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('owl/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('icofont/icofont.min.css')}}">
</head>
<div class="menu close" id="c-menu">
    <div class="menu-kiri" id="t-menu3"></div>
    <div class="menu-kanan" style="background-image: url('icon/bg-img.png')">
        <a href="#" id="t-menu2" class="menu-close"><i class="icofont-close"></i></a>
        <p class="menu-title" href="">TraCKer Kukar <br><span>Tracking Kontak Erat COVID-19 Kutai Kartanegara</span></p>
        <div class="menu-frame">
            <p class="menu-kontak"><i class="icofont-phone"></i> Telepon</p>
            <a href="#" class="menu-value">08627737363</a>
            <p class="menu-kontak"><i class="icofont-envelope"></i> Email</p>
            <a href="#" class="menu-value">gugustugas@gmail.com</a>
            <p class="menu-kontak"><i class="icofont-google-map"></i> Alamat</p>
            <a href="#" class="menu-value">Jl. Awang Sabran BCHK No. 44 Rt. 10</a>
        </div>
        <p class="menu-text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus voluptatum quo maiores nam eos nulla quis. Explicabo quod odit fugiat suscipit natus facilis ab, nobis, voluptas, dicta minima cumque quibusdam?</p>
    </div>
</div>
<body style="background-image: url('icon/bg.png')">
    <div id="app">
        <nav class="navbar navbar-light bg-light navbar-covid">
            <div class="container">
                <a href="#">
                    <img src="{{asset('icon/logo-complt.png')}}" class="navbar-brand" alt="">
                </a>
                <button class="navbar-toggler" id="t-menu" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
          </nav>
        <main>
            @yield('content')
        </main>
    </div>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('owl/owl.carousel.min.js')}}"></script>
    @yield('script')
    <script>
        $('#t-menu').click(function(){
            $('#c-menu').removeClass('close');
        });
        $('#t-menu2').click(function(){
            $('#c-menu').addClass('close');
        });
        $('#t-menu3').click(function(){
            $('#c-menu').addClass('close');
        });
    </script>
</body>
</html>
