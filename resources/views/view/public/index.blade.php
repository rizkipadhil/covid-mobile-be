@extends('view.public.layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row frame">
        <div class="container">
            <div class="col-md-12">
                <div class="intro">
                    <div class="intro_caption">
                        <p class="text text--bg text--black">
                            3 kondisi kapan anda harus melakukan <span class="text--green">scanning</span> QR code :
                        </p>
                        <br>
                        <div class="intro_textframe">
                            <div class="owl-carousel owl-theme">
                                <div class="item">
                                    <p class="text text--green">
                                        <b>1. Jika terjadi pertemuan antar individu</b>
                                    </p>
                                    <p class="text">
                                        The webpack.mix.js file is your entry point for all asset compilation. Think of it as a light configuration wrapper around Webpack. Mix tasks can be chained together to define exactly how your assets should be compiled.
                                    </p>
                                </div>
                                <div class="item">
                                    <p class="text text--green">
                                        <b>1. Jika terjadi pertemuan antar individu</b>
                                    </p>
                                    <p class="text">
                                        The webpack.mix.js file is your entry point for all asset compilation. Think of it as a light configuration wrapper around Webpack. Mix tasks can be chained together to define exactly how your assets should be compiled.
                                    </p>
                                </div>
                                <div class="item">
                                    <p class="text text--green">
                                        <b>1. Jika terjadi pertemuan antar individu</b>
                                    </p>
                                    <p class="text">
                                        The webpack.mix.js file is your entry point for all asset compilation. Think of it as a light configuration wrapper around Webpack. Mix tasks can be chained together to define exactly how your assets should be compiled.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="panel panel--purple">
                            <form method="POST" action="{{ url('login') }}" class="login" enctype="multipart/form-data">
                                @csrf
                                <input class="login_form" type="email" name="email" placeholder="Masukkan Email">
                                <input class="login_form" type="password" name="password" placeholder="Masukkan Password">
                                <input type="submit" class="btn btn--green" value="Masuk &#xea5d;">
                            </form>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="intro_app">
                        <img src="{{asset('icon/app.png')}}" alt="" class="intro_img">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row frame bggreen" style="background-image: url('icon/bg-img.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="tentang_smalltitle">TENTANG APLIKASI</p>
                    <p class="tentang_bigtitle">TraCKer Kukar</p>
                </div>
                <div class="col-md-12 text-center">
                    <p class="text text--white tentang_text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="tentang_flex">
                        <div class="tentang_frame">
                            <p class="tentang_angka">7.733</p>
                            <p class="tentang_var"><i class="icofont-users-alt-1"></i> Pengguna</p>
                        </div>
                        <div class="tentang_frame">
                            <p class="tentang_angka">304</p>
                            <p class="tentang_var"><i class="icofont-building-alt"></i> Tempat</p>
                        </div>
                        <div class="tentang_frame">
                            <p class="tentang_angka">222</p>
                            <p class="tentang_var"><i class="icofont-address-book"></i> Kegiatan</p>
                        </div>
                        <div class="tentang_frame">
                            <p class="tentang_angka">888K</p>
                            <p class="tentang_var"><i class="icofont-qr-code"></i> Kontak</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row frame">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12 text-center">
                        <p class="tentang_smalltitle">PANDUAN PENGGUNAAN</p>
                        <p class="tentang_bigtitle text--green">TraCKer Kukar</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel--purple">
                        <form action="">
                            <select name="faq" class="form-input">
                                <option class="" value="-" selected disabled>-Pilih panduan-</option>
                                <option class="" value="1">1</option>
                            </select>
                        </form>
                        <br>
                        <p class="text text--black">
                            Ada pertanyaan diluar dari daftar di atas? <a href="" class="text text--green">Kontak kami</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="h30"></div>
                </div>
                <div class="col-md-6">
                    <div class="panel">
                        <img src="{{asset('icon/pertanyaan.png')}}" alt="" class="pertanyaan_img">
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="text text--black text--bg">
                        1. Panduan dasar penggunaan aplikasi tracking corona :
                    </p>
                    <br>
                    <p class="text">
                        The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The European languages are members of the same family. Their separate existence is a myth.  Europe uses the same vocabulary. The European languages are members of the same family. For science, music, sport, etc, Europe uses the same vocabulary.
                        <br><br>
                        The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The European languages are members of the same family. Their separate existence is a myth.  Europe uses the same vocabulary. The European languages are members of the same family. For science, music, sport, etc, Europe uses the same vocabulary.The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The European languages are members of the same family. Their separate existence is a myth.  Europe uses the same vocabulary. The European languages are members of the same family. For science, music, sport, etc, Europe uses the same vocabulary.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:30,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
</script>
@endsection