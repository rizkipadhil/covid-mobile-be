@extends('view.public.layouts.app')
@section('content')
<div class="container">
    <div class="login">
        <form method="POST" action="{{ route('login') }}" class="login_form">
        @csrf
            <p class="pertanyaan_title text-center">Masuk dengan Email & Password</p>
            <input id="email" type="email" placeholder="Alamat email" class="form-input form-input2 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <hr>
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                <hr>
            @enderror
            <input id="password" type="password" placeholder="Password" class="form-input form-input2 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
                <hr>
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                <hr>
            @enderror
            <div class="form-button">
                <button id="submit" type="submit" class="btn btn--green">Masuk <i class="icofont-long-arrow-right"></i></button> 
                {{-- <a href="forget4.html">Forget password?</a> --}}
            </div>
        </form>
    </div>
</div>
@endsection
