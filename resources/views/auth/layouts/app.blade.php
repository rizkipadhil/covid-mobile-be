@php
    $logo = \SetFunction::staticData('logoapp', 'file');
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Masuk - TraCKer Kukar</title>
    <link rel="icon" href="{{$logo}}"> 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>
<script src="{{asset('authassets/js/jquery.min.js')}}"></script>
<script src="{{asset('authassets/js/popper.min.js')}}"></script>
<script src="{{asset('authassets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('authassets/js/main.js')}}"></script>
<script>
    $('.logo').css("background-image", "url('{{$logo}}')");
</script>
</body>
</html>