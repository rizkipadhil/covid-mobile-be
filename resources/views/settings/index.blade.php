@extends('view.admin.layouts.app')
@section('tagmenu')
   Setting User
@endsection
@section('content')
	<main class="col-md-12" role="main">
		<div class="row mb-5">
			<div class="col-md-12" style="margin-bottom: 30px;">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
								<h4>Profil</h4>
                                <hr>
                                @php
                                    $data = Auth::user();
                                @endphp
                                @include('global.errorprogram')
                                {!! Form::model($data, ['route' => ['index.profil'], 'method' => 'post','enctype'=>'multipart/form-data','files' => true]) !!}
								@includeIf('settings.include.profil')
								<button type="submit" class="btn btn-primary pull-right cap">Simpan</button>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
                                <h4>Ganti Password</h4>
                                <hr>
                                @include('global.errorprogram')
								{!! Form::open(['route' => ['index.changepassword'],'enctype'=>'multipart/form-data','files' => true]) !!}
								@includeIf('settings.include.changepassword')
								<button type="submit" class="btn btn-primary pull-right cap">Simpan</button>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
