<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('password') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('password') }}</label>
      {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Masukan Password baru']) !!}
      @if (!empty($data))
        <span style="color:red;">Masukan password sebelumnya bila ingin mengganti data / Password baru bila mengganti password!</span>
      @endif
      @if ($errors->has('password'))
        <span style="color:red;">{{$errors->first('password')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('password_confirmation') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('password confirmation') }}</label>
      {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password-confirm']) !!}
      @if ($errors->has('password_confirmation'))
        <span style="color:red;">{{$errors->first('password_confirmation')}}</span>
      @endif
    </div>
  </div>
</div>