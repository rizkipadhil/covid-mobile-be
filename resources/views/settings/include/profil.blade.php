<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('name') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('nama') }}</label>
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('name'))
        <span style="color:red;">{{$errors->first('name')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('email') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('email') }}</label>
      {!! Form::text('email', null, ['class' => 'form-control']) !!}
      @if ($errors->has('email'))
        <span style="color:red;">{{$errors->first('email')}}</span>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      @if(\App\Models\user::find(Auth::user()->id)->file->count() == 0)
      <label>Upload KTP</label>
      <div class="form-group label-floating  {{ $errors->has('foto') ? 'has-error' :'' }}">
        {!! Form::file('foto', ['class' => 'form-control','accept'=>'image/x-png,image/gif,image/jpeg']) !!}
        @if ($errors->has('foto'))
          <span class="text-muted">{{$errors->first('foto')}}</span>
        @endif
      </div>
      @else
        <label>Data KTP Anda :</label>
        <br>
        <img src="{{ \App\Models\user::find(Auth::user()->id)->file->count() > 0 ? asset('uploads/'.\App\Models\user::find(Auth::user()->id)->file->first()->filename) : asset('notfound.png') }}" style="width:30%" alt="">
      @endif
    </div>
  </div>
</div>
