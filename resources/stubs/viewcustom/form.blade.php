{{-- if u want to use Dinamis Form, see the intruction on readme. --}}
@php
    $option = [];
    $option['form'] = $form;
    if (isset($data))
        $option['data'] = $data;
@endphp
@include('view.form.dinamisform', $option)

{{-- manual form --}}
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('nama') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('nama') }}</label>
      {!! Form::text('nama', null, ['class' => 'form-control']) !!}
      @if ($errors->has('nama'))
        <span class="text-muted" style="color:red;">{{$errors->first('nama')}}</span>
      @endif
    </div>
  </div>
</div>
{{-- text editor --}}
<div class="row">
  <div class="col-md-12">
    <div class="form-group label-floating {{ $errors->has('keterangan') ? 'has-error' :'' }}">
      <label class="control-label cap">{{ ucwords('keterangan') }}</label>
      {!! Form::textarea('keterangan', null, ['class' => 'form-control','id'=>'summernote']) !!}
      @if ($errors->has('keterangan'))
        <span class="text-muted" style="color:red;">{{$errors->first('keterangan')}}</span>
      @endif
    </div>
  </div>
</div>
<!-- include summernote css/js -->
@section('scripts')
  <script src="{{ asset('externalasset/summernote/summernote.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#summernote').summernote({
      tabsize: 2,
      popover: {
        image: [],
        link: [],
        air: []
      },
      height: 500
    });
  });
  </script>
@endsection
@section('style')
  <link href="{{ asset('externalasset/summernote/summernote.css') }}" rel="stylesheet">
@endsection